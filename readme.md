# F0F7 - LASER Mammoth
*F0F7 - LASER Mammoth* is a SysEx Librarian to send and receive sound programs to and from your Synthesizer.

## Dev Environment
### Vagrant
Download and install [Virtual Box](https://www.virtualbox.org/) and [Vagrant](https://www.vagrantup.com/)

In project directory:

    vagrant up
    
### /etc/hosts
The Vagrantbox is configured to listen to `33.33.33.10`
The NGINX server listens to `app.test`
You should have a directive in your `/etc/hosts`

    33.33.33.10     app.test

## Package Installation
in project directory:

    npm install

in `/www` :

    bower install

## Build process
in `/`project directory:
    
    grunt

grunt surveils JS and LESS files and starts minification if a file is changed

    grunt dist

grunt dist starts the distribution build

## Frontend 
Frontend is accessible at 
    
    /f0f7/www/fe/

If you have a working Vagrant you can login with login: admin pw: admin

## Backend
The Backend is available here:
https://app.test/be/users/login 
login: admin pw: admin

Btw. it is not necessary to have a working backend when you only want to create a synth profile. Only drawbacks: you cannot login, you cannot save or load patches.

## Create a synth profile
1. copy or create a blank profile file for your synth to `/src/js/devices`
2. add that file to the build script in `/Gruntfile.js `
3. add your device identifier to the head in `/src/js/devices.js`
4. add the manufacturer and device SysEx strings to `/src/js/devices.js` (for device auto detection)
5. add your device identifier again to the bottom of `/src/js/devices.js` (for the dropdown menu)
6. run `grunt dist`