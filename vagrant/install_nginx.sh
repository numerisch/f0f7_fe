#!/usr/bin/env bash
echo "--- Greetings, Professor Falken. ---"

echo "--- Updating packages list ---"
sudo apt update

echo "--- Installing base packages ---"
sudo apt install -y vim curl rsync

echo "--- Installing PHP-specific packages ---"
sudo apt install -y mariadb-server nginx php-fpm php-mysql php-gd php-intl php-cli php-curl php-mbstring php-xml
sudo apt install -y zip unzip php-zip
sudo apt install -y git-core npm ssl-cert
sudo apt install -y composer

echo "--- Copy Skeleton ---"
sudo rsync -rv --exclude=.DS_Store /var/www/html/f0f7/vagrant/skeleton/ /
# Check in pool.d/www.conf : listen = /var/run/php/php7.3-fpm.sock

echo "--- Restarting nginx ---"
sudo ln -s /etc/nginx/sites-available/app.test.conf /etc/nginx/sites-enabled/
sudo service php7.3-fpm restart
sudo service nginx restart

echo "--- Create Database ---"
sudo echo "CREATE DATABASE IF NOT EXISTS f0f7" | mysql -u root
sudo mysql -u root f0f7 < /var/www/html/f0f7/www/be/config/schema/f0f7.sql

echo "--- Add MySQL user and grant ---"
sudo mysql -u root -e "CREATE USER f0f7@localhost IDENTIFIED BY 'root';"
sudo mysql -u root -e "GRANT ALL PRIVILEGES ON f0f7.* TO 'f0f7'@'localhost';"
sudo mysql -u root -e "SET PASSWORD FOR 'f0f7'@'localhost' = PASSWORD('rolfo');"
sudo mysql -u root -e "FLUSH PRIVILEGES;"

echo "--- Create user with permission to edit and add files in /var/www ---";
sudo adduser --disabled-password --ingroup www-data --gecos LASERMammoth f0f7
sudo chown -R www-data:www-data /var/www/html
sudo chmod -R g+rws /var/www/html

# echo "--- Install Bower ---"
# sudo ln -s /usr/bin/nodejs /usr/bin/node
# sudo npm install -g bower

# echo "--- mysql_secure_installation ---"
# mysql_secure_installation

echo "--- All set to go! Would you like to play a game? ---"

# ssl keys
# certbot
# apt install certbot python3-certbot-nginx

