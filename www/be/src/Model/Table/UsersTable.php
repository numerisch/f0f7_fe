<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\Auth\DefaultPasswordHasher;

//Marshall
use Cake\Event\Event;
use ArrayObject;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\HasMany $Banks
 * @property \Cake\ORM\Association\HasMany $Programs
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('users');
        $this->displayField('username');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Banks', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
        $this->hasMany('Programs', [
            'foreignKey' => 'user_id'
        ]);
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
       $sanitizedFields = array('username');
       foreach($sanitizedFields as $field) {
           if(isset($data[$field])) {
               $data[$field] = strip_tags($data[$field]);
           }
       }
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->add('active', 'valid', ['rule' => 'boolean'])
            ->requirePresence('active', 'create')
            ->notEmpty('active');

        $validator
            ->requirePresence('username', 'create')
            ->notEmpty('username', 'A username is required')
            ->add('username', [
                'minLength' => [
                    'rule' => ['minLength', 4],
                    'message' => 'Usernames must be at least 4 characters long.',
                ],
                'maxLength' => [
                    'rule' => ['maxLength', 32],
                    'message' => 'Usernames must be not more than 32 characters long.'
                ],
                'customAlphanumeric' => [
                    'rule' => array('custom', '/^[a-z\d_-]+$/i'),
                    'message' => 'Username must not contain special chars.'
                ]
            ]);

        $validator
            ->requirePresence('email', 'create')
            ->notEmpty('email')
            ->add('email', 'validFormat', [
                'rule' => 'email',
                'message' => 'Email must be valid'
            ]);

        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password')
            ->add('password', [
                'lengthBetween' => [
                    'rule' => ['lengthBetween', 5, 255],
                    'message' => 'Passwords must be between 5 and 255 characters long.',
                ],
            ]);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username'], 'Username is already in use.'));
        $rules->add($rules->isUnique(['email'], 'Email address is already in use.'));
        return $rules;
    }
}
