<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BanksPrograms Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Banks
 * @property \Cake\ORM\Association\BelongsTo $Programs
 *
 * @method \App\Model\Entity\BanksProgram get($primaryKey, $options = [])
 * @method \App\Model\Entity\BanksProgram newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\BanksProgram[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\BanksProgram|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BanksProgram patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\BanksProgram[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\BanksProgram findOrCreate($search, callable $callback = null)
 */
class BanksProgramsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('banks_programs');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Banks', [
            'foreignKey' => 'bank_id'
        ]);
        $this->belongsTo('Programs', [
            'foreignKey' => 'program_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('pos')
            ->allowEmpty('pos');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['bank_id'], 'Banks'));
        $rules->add($rules->existsIn(['program_id'], 'Programs'));
        return $rules;
    }
}
