<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Http\ServerRequest;
use Cake\Core\Configure;

class RecaptchaComponent extends Component {

    public $api = 'v3';
    public $verifyResponse = array();
    public $errMsg = '';
    public $scoreThreshold = 0.3;

    public function initialize(array $config) {
        $this->controller = $this->request->getParam('controller');
    }

    public function verify($token, $ip = null) {
        if(is_null($ip)) {
            $ip = $this->request->clientIp();
        }

        $reCAPTCHA = Configure::read('reCAPTCHA');
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $postVars = array(
            'secret' => $reCAPTCHA[$this->api]['secret'],
            'response' => $token,
            'remoteip' => $ip
        );

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postVars);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec( $ch );
        curl_close($ch);

        $this->verifyResponse = json_decode($response, true);
        return $this->verifyResponse;
    }

    public function isVerified($requestData, $scoreThreshold = null) {

        // if(Configure::read('debug') > 0) {
        //     debug('Debug mode active, recaptcha disabled');
        //     return true;
        // }

        if(!is_null($scoreThreshold)) {
            $this->scoreThreshold = $scoreThreshold;
        }

        if(isset($requestData['g-recaptcha-response'])) {
            $reCAPTCHA = $this->verify($requestData['g-recaptcha-response']);

            if($reCAPTCHA['success'] == true) {
                if($reCAPTCHA['score'] < $this->scoreThreshold) {
                    $logData['request'] =   $requestData;
                    $logData['reCAPTCHA'] = $reCAPTCHA;

                    unset($logData['request']['g-recaptcha-response']);
                    unset($logData['request']['User']['password']);

                    CakeLog::write('recaptcha', json_encode($logData));
                    
                    $this->errMsg = __('You cannot do this because I think you are a bot.');
                } else {
                    return true;
                }
            } else {
                $this->errMsg = __('reCAPTCHA error: %s. Just try again!', $reCAPTCHA['error-codes'][0]);
            }
        } else {
            $this->errMsg = __('Missing reCAPTCHA Token');
        }
        // $this->controller->Flash->error($this->errMsg);
        return false;
    }

    public function isVerifiedV2($requestData) {
        if(isset($requestData['g-recaptcha-response'])) {
            $this->api = 'v2';
            $reCAPTCHA = $this->verify($requestData['g-recaptcha-response']);
            if($reCAPTCHA['success'] == true) {
                return true;
            } else {
                $errorMsg = isset($reCAPTCHA['error-codes'][0]) ? $reCAPTCHA['error-codes'][0] : 'Something is wrong';
                $this->errMsg = sprintf("reCAPTCHA error: %s. Did you solve the captcha?", $errorMsg);
            }
        } else {
            $this->errMsg = __('Missing reCAPTCHA Token');
        }
        // $this->controller->Flash->error($this->errMsg);
        return false;
    }
}
