<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BanksProgramsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BanksProgramsTable Test Case
 */
class BanksProgramsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BanksProgramsTable
     */
    public $BanksPrograms;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.banks_programs',
        'app.banks',
        'app.users',
        'app.programs',
        'app.devices'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BanksPrograms') ? [] : ['className' => 'App\Model\Table\BanksProgramsTable'];
        $this->BanksPrograms = TableRegistry::get('BanksPrograms', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BanksPrograms);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
