angular.module('f0f7App').constant('urls', {
    // BASE: 'https://app.test/fe',
    // API_ENDPOINT: 'https://app.test/be/api'
    BASE: 'https://f0f7.net/fe',
    API_ENDPOINT: 'https://f0f7.net/be/api'
});