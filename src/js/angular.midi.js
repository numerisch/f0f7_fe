angular.module('ngMidi', [])
.factory('WebMIDI', function($window, $q, $localStorage, $log) {
    function _test() {
        return ($window.navigator && $window.navigator.requestMIDIAccess) ? true : false;
    }

    function _connect() {
        var defer = $q.defer();
        var p = defer.promise;

        if(_test()) {
            defer.resolve($window.navigator.requestMIDIAccess({sysex: true}));
        } else {
            defer.reject(new Error('No Web MIDI support'));
        }
        return p;
    }

    return {
        connect: function() {
            return _connect();
        },
        midiAccess : {},
        midiIn : {},
        midiOut : {},
        checkMIDIImplementation : function() {
            if ((typeof(this.midiAccess.inputs) == "function")) {
                $log.log('Old Skool MIDI inputs() code not supported');
            } else {
                $log.log('new MIDIMap implementation');
            }
        },
        selectFirstAvailableInPort : function(success, error) {
            var inputs = this.midiAccess.inputs.values();
            var port = inputs.next();
            if(typeof port.value != 'undefined') {
                success(port);
            } else {
                error();
            }
        },
        selectFirstAvailableOutPort : function(success, error) {
            var outputs = this.midiAccess.outputs.values();
            var port = outputs.next();
            if(typeof port.value != 'undefined') {
                success(port);
            } else {
                error();
            }
        },
        selectInPort : function (id, success, error) {
            var newPort = this.midiAccess.inputs.get(id);
            if ( !newPort ) {
                error();
            } else {
                if(this.midiIn) {
                    this.midiIn.onmidimessage = false; //disable existing callback
                }
                this.midiIn = newPort;
                $log.log("switched to MIDI In Port " + this.midiIn.manufacturer + " " + this.midiIn.name);
                $localStorage.midiInPortId = id;
                 this.midiIn.onmidimessage = function(ev) {
                     success(ev);
                 };
                 return true;
            }
        },
        selectOutPort : function (id, error) {
            var newPort = this.midiAccess.outputs.get(id);
            if ( !newPort ) {
                error();
            } else {
                this.midiOut = newPort;
                $log.log("switched to MIDI Out Port " + this.midiOut.manufacturer + " " + this.midiOut.name);
                $localStorage.midiOutPortId = id;
                return true;
            }
        },
        getAvailableInPorts : function() {
            var availableMIDIInputs = [];
            var inputs=this.midiAccess.inputs.values();
            for ( var input = inputs.next(); input && !input.done; input = inputs.next()) {
                availableMIDIInputs.push({id : input.value.id, name : input.value.manufacturer + ' ' + input.value.name});
            }

            if(availableMIDIInputs.length === 0) {
                availableMIDIInputs = [{name : 'no MIDI in'}];
            }
            return availableMIDIInputs;
        },
        getAvailableOutPorts : function() {
            var availableMIDIOutputs = [];
            var outputs = this.midiAccess.outputs.values();
            for ( var output = outputs.next(); output && !output.done; output = outputs.next()) {
                availableMIDIOutputs.push({id : output.value.id, name : output.value.manufacturer + ' ' + output.value.name});
            }

            if(availableMIDIOutputs.length === 0) {
                availableMIDIOutputs = [{name : 'no MIDI out'}];
            }
            return availableMIDIOutputs;
        },
    };
});
