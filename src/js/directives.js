/**
* Add potential validation fields to form-control form elements (CakePHP 3 with Bootstrap)
*
* Requirement: $scope.errors contains option (validation) errors in default CakePHP 3 JSON response
*/

angular.module('f0f7.directives', [])
.directive('formControl', function($compile) {
    return {
        restrict: 'C', //Match all elements with form-control class
        link: function(scope,element,attrs){

            //Get the model and fieldname (name.field)
            var elements = attrs.ngModel.split(".");
            
            //Create a placeholder for error(s)
            var template = "<span ng-repeat='(type, error) in errors." + elements[1] + "' class='help-block'><p class='text-danger'>{{error}}</p></span>";

            //Compile the template and append it to the parent of the form-control element
            element.parent().append($compile(template)(scope));
        }
    };
})
.directive('navigation', function (routeNavigation) {
    return {
        restrict: "E",
        replace: true,
        templateUrl: "partials/header.html",
        controller: function ($scope) {
            $scope.token = routeNavigation.token;
            $scope.logout = routeNavigation.logout;
            $scope.progressbarPercentage = routeNavigation.getProgressbarPercentage;
            $scope.user_is_valid_pro = routeNavigation.userIsValidPro;
        }
    };
});
