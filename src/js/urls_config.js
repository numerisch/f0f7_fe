angular.module('f0f7App').constant('urls', {
     BASE: 'https://f0f7.ddev.site/fe',
     API_ENDPOINT: 'https://f0f7.ddev.site/be/api'
     // BASE: 'https://f0f7.net/fe',
     // API_ENDPOINT: 'https://f0f7.net/be/api'
 });