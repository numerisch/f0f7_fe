angular.module('f0f7.devices', []).factory('Devices', 
	function(Generic, 
		AccessVirus,
		CasioCz1000,
        CasioCz5000,
		ClaviaNordlead,
		DSIEvolver,
		DSIMopho,
        DSIMophoX4,
		DSIOb6,
		DSIPro2,
		DSIPro12,
		DSIProphet08,
		DSIProphetRev2,
        DSITetra,
		EnsoniqEsq1,
		Groovesynthesis3rdwave,
		// HypersynthXenophone,
		IntellijelPlonk,
		IntellijelRainmaker,
		KawaiK3,
		KorgDw8000,
		KorgMicrokorg,
		MoogVoyager,
		NovationBassstation2,
		NovationSupernova,
		OberheimMatrix6,
		OberheimOb8,
        PioneerDJToraizAS1,
		RolandAlphajuno,
        RolandD50,
		RolandJd800,
		RolandJv80,
		SequentialProphet5,
        SequentialProphet6,
        SequentialPro3,
        SequentialProphetX,
		SequentialTake5,
		SequentialTrigon6,
		StrymonTimeline,
		WaldorfBlofeld,
        WaldorfMicrowave,
		YamahaDx7) {

		var deviceMap = {
			0xF0 : {
				0x00 : {
					0x00 : {
						0x2F : {
							0x04 : OberheimOb8 //Encore OB-8 
						}
						// 	0x21 : {
						// 		0x7F : {
						// 			0x03 : HypersynthXenophone
						// 		}
						// 	}
					},
					0x01 : {
						0x55 : {
							0x12: {
								0x01: StrymonTimeline,
							}
						}
					},
					0x02 : {
						0x14 : {
							0x00 : IntellijelRainmaker,
							0x20 : IntellijelPlonk,
						},
						0x4a : {
							0x01 : Groovesynthesis3rdwave,
						}
					},
					0x20 : {
						0x29 : {
							0x00 : {
								0x33: NovationBassstation2
							},
							0x01 : {
								0x20: NovationSupernova, //SN MK1
								0x21: NovationSupernova, //Nova Laptop
								0x22: NovationSupernova, //SN II KBD
								0x23: NovationSupernova, //NOVA II KBD
								0x24: NovationSupernova, //SN II Rack
							},
						},
						0x33 : {
							0x01 : AccessVirus
						},
						// 0x3c : {
						// 	0x03 : device.elektron_monomachine
						// }
					},
                    0x40 : { //Pioneer DJ ID (second byte)
                        0x05 : { //Pioneer DJ ID (third byte)
                            0x00 : { //Toraz ID (first byte)
                                0x00 : { //Toraz ID (second byte)
                                    0x01 : { //Toraz ID (third byte)
                                        0x08 : PioneerDJToraizAS1
                                    }
                                }
                            }
                        }
                    }
				},
				0x01 : {
					0x20 : DSIEvolver,
					0x23 : DSIProphet08,
					0x25 : DSIMopho,
                    0x26 : DSITetra,
                    0x29 : DSIMophoX4,
					0x2A : DSIPro12,
					0x2C : DSIPro2,
					0x2D : SequentialProphet6,
					0x2E : DSIOb6,
                    0x2F : DSIProphetRev2,
					0x30 : SequentialProphetX,
                    0x31 : SequentialPro3,
                    0x32 : SequentialProphet5,
                    0x35 : SequentialTake5,
                    0x39 : SequentialTrigon6,
				},
				0x04 : {
					0x01 : MoogVoyager
				},
				0x0F : {
					0x02 : EnsoniqEsq1
				},
				0x10 : {
					0x01 : OberheimOb8,
					0x06 : OberheimMatrix6
				},
				0x33 : {
					0x04 : ClaviaNordlead
				},
				0x3E : {
					0x00 : WaldorfMicrowave,
                    0x13 : WaldorfBlofeld,
				},
				0x40 : {
					0x00 : { // This is the MIDI channel for Kawai Devices
						0x00: { // This is called "Group No"
							0x01: KawaiK3
						}
					}
				},
				0x41 : {
                    0x14 : RolandD50,
					0x23 : RolandAlphajuno,
					0x3D : RolandJd800,
					0x46 : RolandJv80
				},
				0x42 : {
					0x03 : KorgDw8000,
					0x58 : KorgMicrokorg,
				},
				0x44 : {
					0x00 : CasioCz1000
				},
				0x43 : {
					0x00 : YamahaDx7,
                    0x01 : YamahaDx7, //SuperMax Mod
				}
			}
		};

		var deviceList = {
			'AccessVirus'          : AccessVirus,
			'CasioCz1000'          : CasioCz1000,
            'CasioCz5000'          : CasioCz5000,
			'ClaviaNordlead'       : ClaviaNordlead,
			'DSIEvolver'           : DSIEvolver,
			'DSIMopho'             : DSIMopho,
            'DSIMophoX4'           : DSIMophoX4,
			'DSIOb6'               : DSIOb6,
			'DSIPro2'              : DSIPro2,
			'DSIPro12'             : DSIPro12,
			'DSIProphet08'         : DSIProphet08,
            'DSITetra'             : DSITetra,
			'DSIProphetRev2'       : DSIProphetRev2,
			'EnsoniqEsq1'          : EnsoniqEsq1,
			'Groovesynthesis3rdwave': Groovesynthesis3rdwave,
			// 'HypersynthXenophone'  : HypersynthXenophone,
			'IntellijelPlonk'  	   : IntellijelPlonk,
			'IntellijelRainmaker'  : IntellijelRainmaker,
			'KawaiK3'			   : KawaiK3,
			'KorgDw8000'  		   : KorgDw8000,
			'KorgMicrokorg'  	   : KorgMicrokorg,
			'MoogVoyager'          : MoogVoyager,
			'NovationSupernova'    : NovationSupernova,
			'NovationBassstation2' : NovationBassstation2,
			'OberheimMatrix6'	   : OberheimMatrix6,
			'OberheimOb8'	       : OberheimOb8,
            'PioneerDJToraizAS1'   : PioneerDJToraizAS1,
			'RolandAlphajuno'      : RolandAlphajuno,
            'RolandD50'            : RolandD50,
			'RolandJd800'          : RolandJd800,
			'RolandJv80'           : RolandJv80,
            'SequentialProphet5'   : SequentialProphet5,
            'SequentialProphet6'   : SequentialProphet6,
            'SequentialPro3'       : SequentialPro3,
			'SequentialProphetX'   : SequentialProphetX,
			'SequentialTake5'      : SequentialTake5,
			'SequentialTrigon6'    : SequentialTrigon6,
			'StrymonTimeline'      : StrymonTimeline,
			'WaldorfBlofeld'       : WaldorfBlofeld,
            'WaldorfMicrowave'     : WaldorfMicrowave,
			'YamahaDx7'            : YamahaDx7,
		};

		return {
			detect : function(md) {
				var device = {};
				if(md[0] == 0xF0) {
					if(md[1] === 0x00) { //new school manufacturers
                        if(typeof deviceMap[md[0]][md[1]][md[2]][md[3]][md[4]][md[5]] != 'undefined') {
                        	if(typeof deviceMap[md[0]][md[1]][md[2]][md[3]][md[4]][md[5]][md[6]] != 'undefined') {
                        		if(typeof deviceMap[md[0]][md[1]][md[2]][md[3]][md[4]][md[5]][md[6]][md[7]] != 'undefined') {
                        			device = deviceMap[md[0]][md[1]][md[2]][md[3]][md[4]][md[5]][md[6]][md[7]];
                        			return device;
                        		}
                        	}
                            device = deviceMap[md[0]][md[1]][md[2]][md[3]][md[4]][md[5]];
                            return device;
                        }
						if(typeof deviceMap[md[0]][md[1]][md[2]][md[3]][md[4]] != 'undefined') {
							device = deviceMap[md[0]][md[1]][md[2]][md[3]][md[4]];
							return device;
						}
					} else { //old school manufacturers
						if(md[1] == 0x41 || md[1] == 0x33 || md[1] == 0x42) { //Roland, Korg and Clavia Special Case : skip device Id
							
							//Roland Alpha Juno Specialcase
							if(typeof deviceMap[md[0]][md[1]][md[4]] != 'undefined') {
								if(md[4] == 0x23) {
									device = deviceMap[md[0]][md[1]][md[4]];
									return device;
								}
							}

							if(typeof deviceMap[md[0]][md[1]][md[3]] != 'undefined') {
								device = deviceMap[md[0]][md[1]][md[3]];
								return device;
							}
						}

						// New style detection - the device itself owns code that allows it to recognize its Sysex
						if (KawaiK3.isOwnSysex(md)) {
							return KawaiK3;
						}

						if(typeof deviceMap[md[0]][md[1]] != 'undefined') {
							if(typeof deviceMap[md[0]][md[1]][md[2]] != 'undefined') {
								device = deviceMap[md[0]][md[1]][md[2]];
								return device;
							}
						}
					}
				}
                return device;
			},
			getOptions : function() {
				var availableDevices = {};
				for (var prop in deviceList){
					if(deviceList.hasOwnProperty(prop)){
						availableDevices[prop] = deviceList[prop].manuf + ' ' + deviceList[prop].model;
					}
				}
				return availableDevices;
			},
			setDevice : function(device) {
				return typeof deviceList[device] != 'undefined' ? deviceList[device] : {};
			}
		};
	}
);
