angular.module('f0f7App').factory('routeNavigation', function($location, $localStorage, Auth, User) {
    var progressbar = {percentage : 0};
    return {
        progressbar: progressbar,
        token: function() {
            return $localStorage.token;
        },
        logout: function () {
            Auth.logout(function () {
                $location.path('/login');
            });
        },
        userIsValidPro: function () {
            return User.is_valid_pro;
        },
        getProgressbarPercentage: function () {
            return progressbar.percentage;
        }
    };
});