angular.module('f0f7.usersController', [])
.controller('UsersController', function ($scope, $location, $localStorage, Auth, Flash, $routeParams) {

		var recaptchaWidgetId;

		$scope.successAlert = function (message) {
			Flash.clear();
		    var id = Flash.create('success', '<strong>Success!</strong> ' + message, 3000, {class: 'alert-top'}, true);
		    // First argument (string) is the type of the flash alert.
		    // Second argument (string) is the message displays in the flash alert (HTML is ok).
		    // Third argument (number, optional) is the duration of showing the flash. 0 to not automatically hide flash (user needs to click the cross on top-right corner).
		    // Fourth argument (object, optional) is the custom class and id to be added for the flash message created.
		    // Fifth argument (boolean, optional) is the visibility of close button for this flash.
		    // Returns the unique id of flash message that can be used to call Flash.dismiss(id); to dismiss the flash message.
		};

		$scope.dangerAlert = function (message) {
			Flash.clear();
		    Flash.create('danger', '<strong>Error!</strong> ' + message, 0, {class: 'alert-top'}, true);
		};

		$scope.warningAlert = function (message) {
			Flash.clear();
		    Flash.create('warning', '<strong>Oh no!</strong> ' + message, 0, {class: 'alert-top'}, true);
		};

		function successAuth(res) {
			$localStorage.token = res.data.token;

			Auth.getUserData(function(res) {
				$scope.successAlert('Welcome !');
				$location.path('/');
			}, function(res) {
				$scope.dangerAlert(res.data.message);
			});
		}

		$scope.login = function () {
			Auth.login($scope.Users, successAuth, function (res) {
				$scope.dangerAlert(res.message);
			});
		};

		$scope.signup = function () {

			$scope.Users['g-recaptcha-response'] = grecaptcha.getResponse(recaptchaWidgetId);

			Auth.signup($scope.Users, function(res) {
				if(res.success) {
					$scope.successAlert(res.data.message);
					$location.path('/login');
				} else {
					$scope.errors = res.data.errors;
					$scope.warningAlert(res.data.message);
					grecaptcha.reset(recaptchaWidgetId);
				}
			}, function (res) {
				$scope.dangerAlert(res.data.message);
			});
		};

		$scope.changeProfileData = function () {
			var formData = $scope.Users;
			if(typeof $scope.Users.password != 'undefined' && $scope.Users.password.length === 0) {
				delete formData.password;
			}
			Auth.editProfile(formData, function() {
				$scope.successAlert('Data successful changed');
			}, function (res) {
				$scope.dangerAlert(res.data.message);
				if(typeof res.data.errors != 'undefined') {
					$scope.errors = res.data.errors;
				}
			});
		};

		$scope.deleteProfile = function() {
			// if (confirm("No further notice! Your profile and all of your banks will be immediately deleted.")) {
				Auth.deleteProfile(function() {
					$scope.successAlert('Profile deleted');
					Auth.logout(function () {
						$location.path('/');
					});
				}, function (res) {
					$scope.dangerAlert(res.data.message);
				});
			// }
		};

		$scope.passwordRequest = function() {
			var formData = {
				username: $scope.username,
			};

			Auth.passwordRequest(formData, function(res) {
				if(res.success) {
					$scope.successAlert(res.data.message);
				} else {
					$scope.warningAlert(res.data.message);
				}
			}, function (res) {
				$scope.dangerAlert(res.data.message);
			});
		};
		$scope.setNewPassword = function() {
			if(typeof $routeParams.id != 'undefined' && typeof $routeParams.hash != 'undefined') {
				var formData = {
					id: $routeParams.id,
					hash: $routeParams.hash,
					password: $scope.password,
				};

				Auth.setNewPassword(formData, function(res) {
					if(res.success) {
						$scope.successAlert(res.data.message);
						$location.path('/login');
					} else {
						var message;
						var errorObj;
						if(typeof res.data.errors != 'undefined') {
							errorObj = res.data.errors.password;
							message = errorObj[Object.keys(errorObj)[0]];
						} else {
							message = res.data.message;
						}
						$scope.warningAlert(message);
					}
				}, function (res) {
					$scope.dangerAlert(res.data.message);
				});
			}
		};

		if($location.$$path == '/register') {
			recaptchaWidgetId = grecaptcha.render('recaptcha-widget', {
			    'sitekey' : '6Le58q0qAAAAAGzC3RIFGoyibK_Tlh_IyYQSi6O1',
			    'theme' : 'dark'
			});
		}

		if($location.$$path == '/profile') {
			Auth.getUserData(function(res) {
				$scope.Users = res.data;
				// $scope.Users.password = '';
			}, function(res) {
				$scope.dangerAlert(res.data.message);
			});
		}

		if($location.$$path == '/set_a_new_password') {
			if($localStorage.token) {
				Auth.logout(function() {
					$scope.warningAlert('Logged you out because I can.');
				});
			}
		}
});
