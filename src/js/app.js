angular.module('f0f7App', [
  'ngRoute',
  'ngMidi',
  'dndLists',
  'ngStorage',
  'f0f7.sysexService',
  'f0f7.controllers',
  'f0f7.pagesController',
  'f0f7.proController',
  'f0f7.usersController',
  'f0f7.devices',
  'f0f7.restService',
  'f0f7.directives',
  'ngFlash',
  'angular-md5',
  'xeditable',
  'angular-loading-bar',
  'ngAnimate',
  'cfp.hotkeys',
  'ngSanitize',
  'ui.bootstrap.position',
  'mwl.confirm',
  'eloquent'
])
.constant('config', {
  accounts : {
    pro : {
      price : 7.90,
      bankmanager : {
        limit : 100
      }
    },
    standard : {
      bankmanager : {
        limit : 3
      }
    }
  }
 })
.value('User', {})
.config(function ($routeProvider, $httpProvider) {

    $routeProvider.
      when('/SysexLibrarian', {
        templateUrl: 'partials/SysexLibrarian.html',
        controller: 'SysexController',
      }).
      when('/SysexLibrarian/:deviceId', {
        templateUrl: 'partials/SysexLibrarian.html',
        controller: 'SysexController',
      }).
      when('/imprint', {
        templateUrl: 'partials/imprint.html',
        controller: 'PagesController',
      }).
      when('/privacy', {
        templateUrl: 'partials/privacy.html',
        controller: 'PagesController',
      }).
      when('/about', {
        templateUrl: 'partials/about.html',
        controller: 'PagesController',
      }).
      when('/pro', {
        templateUrl: 'partials/pro/index.html',
        controller: 'ProController',
      }).
      when('/thank_you_for_supporting_us', {
        templateUrl: 'partials/pro/thank_you_for_supporting_us.html',
        controller: 'PagesController',
      }).
      when('/login', {
        templateUrl: 'partials/users/login.html',
        controller: 'UsersController',
      }).
      when('/register', {
        templateUrl: 'partials/users/register.html',
        controller: 'UsersController',
      }).
      when('/welcome', {
        templateUrl: 'partials/users/welcome.html',
        controller: 'UsersController',
      }).
      when('/profile', {
        templateUrl: 'partials/users/profile.html',
        controller: 'UsersController',
      }).
      when('/password_request', {
        templateUrl: 'partials/users/password_request.html',
        controller: 'UsersController',
      }).
      when('/set_a_new_password', {
        templateUrl: 'partials/users/set_a_new_password.html',
        controller: 'UsersController',
      }).
      when('/optin', {
        resolve: {
          optin: ['optinService', function (optinService) {
            optinService();
          }]
        }
      }).
      otherwise({
        redirectTo: '/SysexLibrarian'
      });

      $httpProvider.interceptors.push(function ($q, $location, $localStorage) {
         return {
             'request': function (config) {
                 config.headers = config.headers || {};
                 if ($localStorage.token) {
                     config.headers.Authorization = 'Bearer ' + $localStorage.token;
                 }
                 config.headers.Accept = 'application/json';
                 config.headers['Content-Type'] = 'application/json';
                 return config;
             },
             'responseError': function (response) {
                 if (response.status === 401 || response.status === 403) {
                     $location.path('/login');
                 }
                 return $q.reject(response);
             }
         };
      });

  })
.run(function(editableOptions, editableThemes, $localStorage, Auth) {
  editableThemes.bs3.inputClass = 'input-sm';
  editableThemes.bs3.buttonsClass = 'btn-sm';
  editableOptions.theme = 'bs3';

  if($localStorage.token) {
    Auth.getUserData(function(res) {
      // console.log(res);
    }, function(res) {
      // console.log(res);
    });
  }
});
