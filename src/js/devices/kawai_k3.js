/*jshint esversion: 6 */
/*jshint bitwise: false*/
/*globals angular */

//
// Kawai K3 module for LaserMammoth.
//
// Copyright 2020 by Christof Ruch
//
// The K3 has two sysex formats - a single program with storage place 0-99, or a bank dump with 50 patches
// We will query for a full bank, but always store single programs in the database!
//
// Problematic is that the MIDI channel of the device is always encoded in each message, and needs to be adapted
// to the currently connected synth.

angular.module('f0f7.devices').factory('KawaiK3', function($log, $timeout, Sysex, WebMIDI) {
    'use strict';
    const obj = {
        id 			: 'KawaiK3',
        manuf 		: 'Kawai',
        model 		: 'K3/K3M',
        hints 		: [
            'The Kawai K3 has no volatile edit buffer, therefore we use program slot 50 to receive data sent.' +
            ' Be careful, anything stored there will be overwritten when you send a patch to the synthesizer!',
        ],
        // Some constant definitions particular to the Kawai K3
        sysexFunctions: {
            ONE_BLOCK_DATA_REQUEST: 0,
            ALL_BLOCK_DATA_REQUEST: 1,
            PARAMETER_SEND: 16,
            ONE_BLOCK_DATA_DUMP: 32,
            ALL_BLOCK_DATA_DUMP: 33,
            WRITE_COMPLETE: 64,
            WRITE_ERROR: 65,
            WRITE_ERROR_BY_PROTECT: 66,
            WRITE_ERROR_BY_NO_CARTRIDGE: 67,
            MACHINE_ID_REQUEST: 96,
            MACHINE_ID_ACKNOWLEDGE: 97,
            INVALID_FUNCTION: 255
        },
        bytesPerProgram: 70, // These are 4 bit nibbles, to interpret we would need to de-nibble
        params : {
            banks : {
                bank1 : {label : 'Internal', address : 0x00},
                bank2 : {label : 'Cartridge', address : 0x01},
            },
            patchesPerBank : 50,
            transferPause : 600,
            midiChannel: 4, // Zero based MIDI channel. TODO we need to find out how to configure this by the user?
            parserConfig : {
                dna : {
                    from : 0,
                    to : 70
                }
            }
        },
        sendToBuffer : function(program) {
            // Remember the Kawai K3 has no Edit Buffer, so we send to program place 50 (49)
            let midiData = program.oneBlockDataDump;
            if (!Array.isArray(midiData)) {
                midiData = Sysex.objectToArray(midiData);
            }
            midiData[2] = obj.params.midiChannel & 0x0f;
            midiData[6] = 49;
            Sysex.sendSysex(midiData, WebMIDI.midiOut);

            // Select some other program, so we can select the 49 soon
            let prgChange = [0xC0 | obj.params.midiChannel, 0 & 0x7F];
            Sysex.sendSysex(prgChange, WebMIDI.midiOut);

            // Now select the newly sent program, otherwise the Kawai K3 will play still the old sound
            prgChange = [0xC0| obj.params.midiChannel, 49 & 0x7F];
            Sysex.sendSysex(prgChange, WebMIDI.midiOut);
        },
        sendProgramToBank : function(program, bank, prgNo) {
            //Send Bulk Data
            let midiData = this.reorderProgram(program, bank, prgNo);
            Sysex.sendSysex(midiData, WebMIDI.midiOut);
        },
        reorderProgram : function(program, bank, prgNo) {
            let midiData = program.oneBlockDataDump;
            if (!Array.isArray(midiData)) {
                midiData = Sysex.objectToArray(midiData);
            }
            midiData[2] = obj.params.midiChannel & 0x0f;
            switch (bank) {
                case 0:
                    midiData[6] = prgNo; break;
                case 1:
                    midiData[6] = prgNo + 50; break;
                default:
                    $log.error(`Invalid argument in function 'reorder Program':${bank.toString()}`);
                    return {};
            }

            return midiData;
        },
        retrieveBank : function(bank) {
            let requestMessage = this.buildSysexFunctionMessage(this.sysexFunctions.ALL_BLOCK_DATA_REQUEST, bank, 4);
            Sysex.sendSysex(requestMessage, WebMIDI.midiOut);
        },
        extractPatchNames : function(midiData) {
            let program = [];
            let data = Sysex.objectToArray(midiData);
            if (this.isOwnSysex(data)) {
                switch (this.determineSysexFunction(data)) {
                    case this.sysexFunctions.ALL_BLOCK_DATA_DUMP:
                        // There are 50 programs in one block dump
                        for (let i = 0; i < 50; i++) {
                            let startIndex = 7 + i * 35 * 2;
                            program.push(this.buildProgramFromSysExData(data, startIndex, i));
                        }
                        break;
                    case this.sysexFunctions.ONE_BLOCK_DATA_DUMP:
                        data = Sysex.objectToArray(midiData);
                        program.push(this.buildProgramFromSysExData(data, 7, 0));
                        break;
                }
            }
            return program;
        },
        getMaxNameLength : function() {
            return 0;
        },
        buildProgramFromSysExData: function(data, startIndex, count) {
          let program = { name: 'Kawai K3 - ' + (count + 1).toString() };
          let nibbledData = data.slice(startIndex, startIndex + this.bytesPerProgram );
          let denibbledData = Sysex.denibbleHighNibbleFirst(nibbledData);
          let check = this.calcCheckSum(denibbledData, 34);
          if (check !== denibbledData[34]) {
              console.log('Checksum error in data retrieved from Kawai K3!');
              return {};
          }
          program.data = denibbledData;
          program.dna = Sysex.createDNA(denibbledData, 0, 34);
          let oneBlock = this.buildSysexFunctionMessage(this.sysexFunctions.ONE_BLOCK_DATA_DUMP, count);
          program.oneBlockDataDump = oneBlock.slice(0, oneBlock.length - 1); // Drop the 0xF7 at the end
          program.oneBlockDataDump = program.oneBlockDataDump.concat(nibbledData);
          program.oneBlockDataDump.push(0xf7);
          return program;
        },
        buildSysexFunctionMessage: function(functionNo, subCommand) {
            return [0xF0, 0x40, obj.params.midiChannel, functionNo, 0x00, 0x01, subCommand, 0xF7];
        },
        determineSysexFunction: function(message) {
            if (this.isOwnSysex(message) && message.length > 3) {
                return message[3];
            }
            return -1;
        },
        isOwnSysex: function(message) {
            return (message[1] === 0x40) &&
                ((message[2] & 0xf0) === 0x00) &&
                (message[4] === 0) &&
                (message[5] === 1);
        },
        calcCheckSum: function(data, numberOfItems) {
            // The Kawai K3 uses 8 bit check-sums, not 7 bit, as it calculates it only after denibble!
            let sum = 0;
            for (let i = 0; i < numberOfItems; i++) {
                sum += data[i];
            }
            return sum & 0xff;
        }
    };
    return obj;
});
