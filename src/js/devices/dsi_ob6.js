angular.module('f0f7.devices').factory('DSIOb6', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'DSIOb6',
		manuf 		: 'Dave Smith Instruments',
		model 		: 'OB-6 [beta]',
		hints 		: [
			'This profile is not well tested. Please report errors.',
			'In <strong>GLOBALS</strong> menu set <strong>MIDI SysEx</strong> to <strong>Mid</strong>i or <strong>USb</strong> depending on which connection you use.',
			'You can only transfer programs from bank 0 to bank 4, higher banks are readable only (ROM).'
		],
		params : {
			banks : {
				bank1 : {label : 'OB-6 / Bank 0', address : 0x00},
				bank2 : {label : 'OB-6 / Bank 1', address : 0x01},
				bank3 : {label : 'OB-6 / Bank 2', address : 0x02},
				bank4 : {label : 'OB-6 / Bank 3', address : 0x03},
				bank5 : {label : 'OB-6 / Bank 4', address : 0x04},
			},
			patchesPerBank : 100,
			transferPause : 400,
			parserConfig : {
				rawData : {
					from: 6,
					to: 1177 //1171 bytes packed, 1024 expanded
				},
				name : {
					from : 107,
					to : 126
				},
				dna : {
					from : 0,
					to : 1024
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			midiData = Array.from(midiData);
			midiData[3] = 0x03; 	// Edit Buffer Data
			midiData.splice(4, 2);	// Unset Bank and Prg
			midiData = Sysex.convertToUint8(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[3] = 0x02; 	// Program Data Dump
			midiData[4] = bank;		// 0 - 9
			midiData[5] = prgNo;	// 0 - 99

			return midiData;
		},
		retrieveBank : function(bank) {
			var header = [0xF0, 0x01, 0x2E, 0x05];
			Sysex.requestAllProgramsOfBank(bank, WebMIDI.midiOut, header, this.params.patchesPerBank, this.params.transferPause);
		},
		extractPatchNames : function(midiData) {
			midiData = Sysex.objectToArray(midiData);
			var unpackedData = this.unpackMidiData(midiData);
			var program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			program.data = midiData;
			return [program];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 22; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var newNameArray = Sysex.createPaddedNameArray(nameLength, newName);

			var midiData = Sysex.objectToArray(program.data);
			var header = midiData.slice(0, this.params.parserConfig.rawData.from);
			var unpackedData = this.unpackMidiData(midiData);

			unpackedData = Sysex.changeNameinMidiData(newNameArray, nameLength, unpackedData, this.params.parserConfig);
			program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			
			var packedData = Sysex.packDSISysex(unpackedData);
			program.data = header.concat(packedData, 0xF7);
			return program;
		},
		unpackMidiData : function(midiData) {
			var packedData = midiData.slice(this.params.parserConfig.rawData.from, this.params.parserConfig.rawData.to);
			var unpackedData = Sysex.unpackDSISysex(packedData);
			// Sysex.dumpData(unpackedData, 16);
			return unpackedData;
		}
	};
	return obj;
});
