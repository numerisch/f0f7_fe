angular.module('f0f7.devices').factory('WaldorfMicrowave', function($log, $timeout, Sysex, WebMIDI) {
	var obj = {
		id 			: 'WaldorfMicrowave',
		manuf 		: 'Waldorf Music',
		model 		: 'MicroWave',
		hints 		: [
			'In <strong>Device Param.</strong> menu set <strong>Dev. Number</strong> to <strong>000</strong>',
            'Works with OS V1.25 and V2.0 on a MicroWave 1',
            '<em>Download SysEx</em> will only work if the bank has exactly 64 programs',
            'Should also work with the Waldorf M',
		],
		params : {
            deviceId    : 0x00,
            singleProgramsCannotBeTransmitted : true,
			banks : {
				bank1 : {label : 'Internal Bank (A+B)', address : 0x10}
			},
			patchesPerBank : 64,
			transferPause : 15,
			parserConfig : {
				name : {
					from : 153, //pos with header
					to : 168
				},
				dna : {
					from : 5,
					to : 185
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			// Sysex.dumpData(midiData, 16);

			midiData[3] = this.params.deviceId;
            midiData[4] = 0x42; //BPRD Dump

            var rawData = this.getRawData(midiData);
			midiData[midiData.length-2] = Sysex.calculateWaldorfChecksum(rawData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
        sendProgramToBank : function(program, bank, prgNo) {
            var midiData = this.reorderProgram(program, bank, prgNo);
            // Sysex.dumpData(midiData, 16);

            if(midiData.length > 0) {
                Sysex.sendSysex(midiData, WebMIDI.midiOut);
            }
        },
        reorderProgram : function(program, bank, prgNo) {
            if(prgNo === 0) {
                this.params.buffer = [];
            }
            var rawData = this.getRawData(Sysex.objectToArray(program.data));
            this.params.buffer = this.params.buffer.concat(rawData);

            if(prgNo == (this.params.patchesPerBank-1)) {
                var header = [0xF0, 0x3E, 0x00, this.params.deviceId, 0x50];

                var checksum = Sysex.calculateWaldorfChecksum(this.params.buffer);
                var eox = [checksum, 0xF7];
                
                var midiData = header.concat(this.params.buffer, eox);
                // console.log(checksum);

                this.params.buffer = [];
                return midiData;
            }
            return [];
        },
		retrieveBank : function(bank) {
            //Microwave Sound-Program Bank Dump request
            var midiData = [0xF0, 0x3E, 0x00, this.params.deviceId, bank, 0x00, 0xF7];
            Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
        extractPatchNames : function(midiData) {
            var program = [];

            midiData = Sysex.objectToArray(midiData);

            switch(midiData[4]) {
                case 0x42 : program = this.extractSinglePatch(midiData); break; //42  BPRD BPR Dump
                case 0x50 : program = this.extractMultiDump(midiData); break; //50  BPBD BPR Bank Dump
            }
            
            return program;
        },
        extractSinglePatch : function(midiData) {
            if(midiData.length == 187) {
                var program = Sysex.extractPatchName(midiData, this.params.parserConfig);
                return [program];
            } else {
                return [];
            }
        },
        extractMultiDump : function(midiData) {
            var rawData = midiData.slice(5, (midiData.length-2));

            var chunk = 180;
            var i,j,data,checksum,eox;
            var patchNames = [];
            var program = [];
            var header = [0xF0, 0x3E, 0x00, this.params.deviceId, 0x42];

            for (i=0,j=rawData.length; i<j; i+=chunk) {
                data = rawData.slice(i,i+chunk);

                // Sysex.dumpData(data, 16);

                checksum = Sysex.calculateWaldorfChecksum(data);
                eox = [checksum, 0xF7];

                data = header.concat(data, eox);
                program = Sysex.extractPatchName(data, this.params.parserConfig);

                patchNames.push(program);
            }
            return patchNames;
        },
        getRawData : function(midiData) {
            return midiData.slice(5, 185);
        },
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return ?; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			return Sysex.renameProgram(newName, nameLength, program.data, this.params.parserConfig);
		}
	};
	return obj;
});
