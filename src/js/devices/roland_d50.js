angular.module('f0f7.devices').factory('RolandD50', function($log, $timeout, Sysex, WebMIDI) {
    var obj = {
        id          : 'RolandD50',
        manuf       : 'Roland',
        model       : 'D-50',
        hints       : [
            'You cannot request programs from <em>F0F7</em>, you must <span class="text-warning">start <em>Bulk Dump</em> and <em>Bulk Load</em> via <strong>DATA TRANSFER</strong></span> menu on the D-50',
            'MIDI CHannel must be set to <strong>1</strong>',
            'For complete MIDI dumps via handshake the D-50 must be connected with <strong>two MIDI cables</strong>',
            'In <strong>TUNE/FUNCTION</strong> menu set <strong>Protect</strong> to <strong>OFF</strong>',
            'In <strong>MIDI-3</strong> menu set <strong>Exclu</strong> to <strong>ON</strong>',
            'The <em>MASTERBOARD</em> list must contain exactly 64 programs',
            'Data for the additional Reverb programs 17-32 will be ignored',
            'Might work also with <em>Roland D-550</em> and <em>Roland D-05</em>'
        ],
        params : {
            deviceId    : 0x00, //D50 Midi Basic Channel = 1
            modelId     : 0x14, //D50
            singleProgramsCannotBeTransmitted : true,
            banksCannotBeRequested : true,
            handshakeMode : false,
            dataSets : [],
            banks : {
                bank1 : {label : 'Internal', address : 0x00},
            },
            patchesPerBank : 64,
            buffer      : [],
            transferPause : 20,
            parserConfig : {
                name : {
                    from : 384,
                    to : 402
                },
                dna : {
                    from : 0,
                    to : 447
                }
            }
        },
        sendToBuffer : function(program) {
            var header = [0xF0, 0x41, this.params.deviceId, this.params.modelId, 0x12];
            var address = 0x000000;
            var addrObj = {};

            var rawData = Sysex.objectToArray(program.data);

            var checksum = 0;
            var midiData = [];

            var i,j,data;
            var chunk = 256;
            for (i=0,j=rawData.length; i<j; i+=chunk) {
                addrObj = this.numberToRolandAddrObj(address);
                data = rawData.slice(i,i+chunk);
                checksum = 0x00; //Checksum not necessary for Buffer programs

                midiData = [].concat(header, [addrObj.hh, addrObj.mm, addrObj.ll], data, [checksum], [0xF7]);
                address += 0x100;
                
                Sysex.sendSysex(midiData, WebMIDI.midiOut);
            }
        },
        sendProgramToBank : function(program, bank, prgNo) {
            var midiData = this.reorderProgram(program, bank, prgNo, this.params.handshakeMode);
            if(midiData.length > 0) {
                Sysex.sendSysex(midiData, WebMIDI.midiOut);
            }
            else if(this.params.dataSets.length > 0) {
                this.sendDAT();
            }
        },
        reorderProgram : function(program, bank, prgNo, handshakeMode) {
            handshakeMode = (typeof handshakeMode !== 'undefined') ?  handshakeMode : false;
            this.params.buffer = this.params.buffer.concat(program.data);

            if(this.params.buffer.length == (448 * this.params.patchesPerBank)) {
                var cmd = handshakeMode == true ? 0x42 : 0x12;
                var header = [0xF0, 0x41, this.params.deviceId, this.params.modelId, cmd];
                
                var address = 0x020000;
                var addrObj = {};

                var rawData = this.params.buffer;

                var reverbData = Array(6016).fill(0); //init memory with 16 reverb patches á 376 byte
                rawData = [].concat(rawData,reverbData);

                var checksum = 0;
                var midiData = [];

                var i,j,data;
                var chunk = 256;

                for (i=0,j=rawData.length; i<j; i+=chunk) {
                    addrObj = this.numberToRolandAddrObj(address);
                    data = rawData.slice(i,i+chunk);

                    checksum = Sysex.calculateRolandChecksum([].concat([addrObj.hh, addrObj.mm, addrObj.ll], data));

                    if(handshakeMode == true) {
                        this.params.dataSets.push([].concat(midiData, header, [addrObj.hh, addrObj.mm, addrObj.ll], data, [checksum], [0xF7]));
                    } else {
                        midiData = [].concat(midiData, header, [addrObj.hh, addrObj.mm, addrObj.ll], data, [checksum], [0xF7]);
                    }
                    address += 0x100;
                }
                this.params.buffer = [];
                return midiData;
            } else {
                return [];
            }
        },
        retrieveBank : function(bank) {
            //Not supported - dump must be initiated from D-50
        },
        extractPatchNames : function(midiData) {
            var program = [];
            midiData = Sysex.objectToArray(midiData);
            switch(midiData[4]) {
                case 0x12 : program = this.processDataSet(midiData); break; //DT1 - Data Set
                case 0x40 : this.sendAcknowledge(); break; //WSD - D50 has send a "Want to sent"
                case 0x41 : this.params.handshakeMode = true; break; //RQD - D50 has send a Request for data. Enable Handshake. todo: auto-send dat
                case 0x42 : program = this.processDataSet(midiData);  //DAT - Data Set (from Handshake)
                            this.sendAcknowledge();
                            break;
                case 0x43 : this.sendDAT(); break; //ACK - D-50 has received data and wants more(from Handshake)
                case 0x4F : this.exitHandshakeMode(); break; //RJC - D-50 has send Rejection
            }
            return program;
        },
        sendAcknowledge : function() {
            var acknowledge = [0xF0, 0x41, this.params.deviceId, this.params.modelId, 0x43, 0xF7];
            Sysex.sendSysex(acknowledge, WebMIDI.midiOut);
        },
        sendEndOfData : function() {
            var eod = [0xF0, 0x41, this.params.deviceId, this.params.modelId, 0x45, 0xF7];
            Sysex.sendSysex(eod, WebMIDI.midiOut);
        },
        sendDAT : function() {
            if(this.params.dataSets.length > 0) {
                var dat = this.params.dataSets.shift();
                Sysex.sendSysex(dat, WebMIDI.midiOut);
            } else {
                this.exitHandshakeMode();
                this.sendEndOfData();
            }
        },
        exitHandshakeMode : function() {
            console.log("Exit Handshake Mode");
            this.params.dataSets.length = [];
            this.params.handshakeMode = false;
        },
        processDataSet : function(midiData) {
            if(midiData.length == 138) { //clear buffer on last msg part
                this.params.buffer = [];
                return [];
            }

            var rawData = midiData.slice(8, (midiData.length-2));
            this.params.buffer = this.params.buffer.concat(rawData);

            if(this.params.buffer.length == (448 * this.params.patchesPerBank)) {
                return this.processBulk(this.params.buffer);
            } else {
                return [];
            }
        },
        processBulk : function(paramsBuffer) {
            var parserConfig = this.params.parserConfig;
            var i,j,data;
            var nameArray = [];
            var patchName = '';
            var dna = '';
            var patchNames = [];
            var chunk = 448;
            for (i=0,j=paramsBuffer.length; i<j; i+=chunk) {
                data = paramsBuffer.slice(i,i+chunk);
                if(data.length == chunk) {
                    nameArray = data.slice(parserConfig.name.from, parserConfig.name.to);
                    patchName = this.nameArrayToString(nameArray);
                    dna = Sysex.createDNA(data, parserConfig.dna.from, parserConfig.dna.to);
                    patchNames.push({name: patchName, dna:dna, data:data});
                }
            }
            return patchNames;
        },
        getMaxNameLength : function() {
            return this.params.parserConfig.name.to - this.params.parserConfig.name.from;
        },
        renameProgram : function(newName, program) {
            var parserConfig = this.params.parserConfig;
            var nameLength = this.getMaxNameLength();
            var newNameArray = this.createPaddedArrayFromCharset(newName);

            var midiData = Sysex.objectToArray(program.data);

            for (var i = 0; i < newNameArray.length; i++) {
                midiData[i+parserConfig.name.from] = (midiData[i+parserConfig.name.from] & 0xC0) | (newNameArray[i] & 0x3F);
            }
            
            var renamedPrg = {};

            renamedPrg.name = this.nameArrayToString(newNameArray);
            renamedPrg.dna = Sysex.createDNA(midiData, parserConfig.dna.from, parserConfig.dna.to);
            renamedPrg.data = midiData;

            return renamedPrg;
        },
        createPaddedArrayFromCharset : function (str) {
            var nameLength = this.getMaxNameLength();
            var bytes = [];
            var charCode, index;
            
            for (var i = 0; i < nameLength; i++) {
              if(typeof str[i] === 'undefined') {
                index = 0; //index of space
              } else {
                index = this.rolandCharset.indexOf(str[i]);
              }
              bytes.push(index);
            }
            return bytes;
        },
        mapRolandHexToASCII : function(hex) {
            if(typeof this.rolandCharset[hex] !== 'undefined') {
                return this.rolandCharset[hex];
            } else {
                return ' ';
            }
        },
        nameArrayToString : function(nameArray) {
            var patchName = '';
            for(var i=0; i<nameArray.length; i++) {
                patchName += this.mapRolandHexToASCII(nameArray[i] & 0x3F);
            }
            patchName = patchName.trim();
            if(patchName.length === 0) {
                patchName = 'no name';
            }
            return patchName;
        },
        /**
         * [numberToRolandAddrObj Converts a number to Roland specific 7Bit 3 part addres object]
         * @param  integer number [description]
         * @return object        [description]
         */
        numberToRolandAddrObj : function(number) {
            var addrObj = {};

            addrObj.ll = number & 0x7F;
            addrObj.mm = (number >> 7) & 0x7F;
            addrObj.hh = (number >> 14) & 0x7F;

            var mystery = ((number >> 16) & 0x7F) * 3; //I don't know why this is working
            addrObj.hh -= mystery;

            addrObj.hex = {};
            addrObj.hex.hh = addrObj.hh.toString(16);
            addrObj.hex.mm = addrObj.mm.toString(16);
            addrObj.hex.ll = addrObj.ll.toString(16);
            return addrObj;
        },
        rolandCharset : [
            ' ', 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
            'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
            '1','2','3','4','5','6','7','8','9','0','-'
        ],
    };
    return obj;
});
