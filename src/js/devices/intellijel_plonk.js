angular.module('f0f7.devices').factory('IntellijelPlonk', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'IntellijelPlonk',
		manuf 		: 'Intellijel',
		model 		: 'Plonk',
		hints 		: [
			'Click the <strong>Request Programs</strong> button below to receive all presets from Plonk.',
			'or',
			'Click the <strong>CONFIG</strong> button and turn the <strong>ENCODER</strong> till the display reads "Send Presets".',
			'Click the <strong>ENCODER</strong>. If you haven’t yet connected the USB cable to the back of the module the display will read "Connect USB". Once the USB cable is connected it will read "Click ENC to begin".',
			'Click the <strong>ENCODER</strong> to begin SysEx transmission.',
			'Once LASER Mammoth has received 128 messages and the display reads "Presets Sent" the preset transfer process is complete. You can now push any button to exit this menu.'
		],
		params : {
			banks : {
				bank1 : {label : 'Plonk Bank', address : 0x01},
			},
			patchesPerBank : 128,
			transferPause : 350,
			parserConfig : {
				rawData : {
					from: 7,
					to: 117
				},
				name : {
					from : 1,
					to : 11
				},
				dna : {
					from : 7,
					to : 117
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			midiData = Array.from(midiData);
			midiData[5] = 0x03; 	// Edit Buffer Data
			midiData = Sysex.convertToUint8(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			// To upload a single preset (from the computer to the Plonk module):
			// 0xF0, 0x00, 0x02, 0x14, 0x00, 0xNN, 0xDD ... 0xCC 0xF7
			// where 0xDD ... are the data bytes containing the preset parameter values.
			var midiData = Sysex.objectToArray(program.data);
			midiData[6] = prgNo;

			var unpackedData = this.unpackMidiData(midiData);
			midiData[midiData.length-2] = Sysex.calculateRolandChecksum(unpackedData);

			return midiData;
		},
		retrieveBank : function(bank) {
			//To download all 128 presets at once: 0xF0, 0x00, 0x02, 0x14, 0x02, 0xF7
			//There will be a series of 128 responses as when downloading a single preset. 
			var midiData = [0xF0, 0x00, 0x02, 0x14, 0x20, 0x02, 0xF7];
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		extractPatchNames : function(midiData) {
			midiData = Sysex.objectToArray(midiData);
			var unpackedData = this.unpackMidiData(midiData);
			var program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			program.data = midiData;
			return [program];
		},
		getMaxNameLength : function() {
			return 11;
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var newNameArray = Sysex.createPaddedNameArray(nameLength, newName);

			var midiData = Sysex.objectToArray(program.data);
			var header = midiData.slice(0, this.params.parserConfig.rawData.from);
			var unpackedData = this.unpackMidiData(midiData);

			unpackedData = Sysex.changeNameinMidiData(newNameArray, nameLength, unpackedData, this.params.parserConfig);

			var packedData = Sysex.packDSISysex(unpackedData);

			var checksum = Sysex.calculateRolandChecksum(unpackedData);
			var eox = [checksum, 0xF7];

			midiData = header.concat(packedData, eox);
			program = this.extractPatchNames(midiData);

			return program[0];
		},
		unpackMidiData : function(midiData) {
			this.setParseConfig(midiData);
			var packedData = midiData.slice(this.params.parserConfig.rawData.from, this.params.parserConfig.rawData.to);
			return Sysex.unpackDSISysex(packedData);
		},
		setParseConfig : function(midiData) {
			this.params.parserConfig.rawData.to = this.params.parserConfig.dna.to = midiData.length - 2;
		}
	};
	return obj;
});
