angular.module('f0f7.devices').factory('Generic', function($log, Sysex, WebMIDI) {
	var obj = {
		id		: 'Generic',
		manuf	: 'Zumbitsu',
		model	: 'Generic',
	};
	return obj;
});
