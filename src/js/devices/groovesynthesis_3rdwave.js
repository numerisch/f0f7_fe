angular.module('f0f7.devices').factory('Groovesynthesis3rdwave', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'Groovesynthesis3rdwave',
		manuf 		: 'Groove Synthesis',
		model 		: '3rd Wave',
		hints 		: [
			'In <strong>GLOBAL SETTINGS</strong> menu set <strong>MIDI Sysex Enable</strong> to <strong>On</strong>',
			'In <strong>GLOBAL SETTINGS</strong> menu set <strong>MIDI in cable</strong> and <strong>MIDI out cable</strong> to <strong>usb</strong>',
			'<strong>Attention:</strong> to prevent MIDI loops be sure to select <strong>usb</strong> instead of <strong>usb+midi</strong>',
			'There is a lot of data to transmit so expect transfer speed to be slow, even with USB',
			'Tested with OS v1.6',
		],
		params : {
			banks : {
				bank1 : {label : '3rd Wave Bank 1', address : 0x00},
                bank2 : {label : '3rd Wave Bank 2', address : 0x01},
                bank3 : {label : '3rd Wave Bank 3', address : 0x02},
                bank4 : {label : '3rd Wave Bank 4', address : 0x03},
                bank5 : {label : '3rd Wave Bank 5', address : 0x04},
			},
			patchesPerBank : 100,
			transferPause : 5000,
			parserConfig : {
				rawData : {},
				name : {},
				dna : {}
			},
			bankRequestInProgress : 0,
			currentRequestedBank : 0,
			currentRequestedPrgNo : 0
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			midiData = Array.from(midiData);

			var requestType = this.getRequestType(midiData);

			midiData[5] = 0x03; 	// Edit Buffer Data

			if(requestType == 'Program data') {
				midiData.splice(6, 2);	// Unset Bank and Prg
			}

			midiData = Sysex.convertToUint8(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);
			var requestType = this.getRequestType(midiData);

			if(requestType == 'Edit buffer data') {
				midiData.splice(6, 0, bank, prgNo); //extend array with bank and Prg
			}

			midiData[5] = 0x02; 	// Program Data Dump
			midiData[6] = bank;		// Bank 0 - 4
			midiData[7] = prgNo;	// Program 0 - 99

			return midiData;
		},
		retrieveBank : function(bank) {
			if(this.params.bankRequestInProgress == 0) {
				this.params.bankRequestInProgress = 1;
				this.params.currentRequestedBank = bank;
				this.params.currentRequestedPrgNo = 0;
				this.retrieveProgram();
			}
		},
		retrieveProgram : function() {
			var header = [0xF0, 0x00, 0x02, 0x4a, 0x01, 0x05];
			var dumpRequest = header.concat([this.params.currentRequestedBank, this.params.currentRequestedPrgNo, 0xF7]);
			$log.info("Send dumpRequest for PrgNo " + this.params.currentRequestedPrgNo);
			Sysex.sendSysex(dumpRequest, WebMIDI.midiOut);
		},
		extractPatchNames : function(midiData) {
			midiData = Sysex.objectToArray(midiData);
			var program = this.extractPatchName(midiData);
			program.data = midiData;

			if(this.params.bankRequestInProgress == 1) {

				if(this.params.currentRequestedPrgNo < (this.params.patchesPerBank-1)) {
					this.params.currentRequestedPrgNo++;
					this.retrieveProgram();
				} else {
					$log.info("Bank dumpRequest completed");
					this.params.bankRequestInProgress = 0;
				}

			}

			return [program];
		},
		getMaxNameLength : function() {
			return 32;
		},
		renameProgram : function(newName, program) {
			var parserConfig = this.setParserConfig(program.data);
			var nameLength = this.getMaxNameLength();
			var newNameArray = Sysex.createPaddedNameArray(newName.length, newName);

			newNameArray = this.convertToMsbLsbHex(newNameArray);
			var midiData = Sysex.objectToArray(program.data);

			midiData = Sysex.changeNameinMidiData(newNameArray, (nameLength * 4), midiData, parserConfig);
			program = this.extractPatchName(midiData);

			return program;
		},
		extractPatchName : function(midiData) {
			var data = angular.fromJson(angular.toJson(midiData));
			var patchName = '';
            var char;

            var pNoMSB, pNoLSB, pValueMSB, pValueLSB;

            var parserConfig = this.setParserConfig(data);

			for(var k=parserConfig.name.from; k<=parserConfig.name.to; k += 4) {

				if(typeof data[k] !== 'undefined') {

					pNoMSB = data[k];
					pNoLSB = data[k+1];
					pValueMSB = data[k+2];
					pValueLSB = data[k+3];

					if(pValueLSB < 32 || pValueLSB > 127) {
						char = 32;
					} else {
                        char = pValueLSB;
                    }

					patchName += String.fromCharCode(char);
				}
			}
			var dna = Sysex.createDNA(midiData, parserConfig.dna.from, parserConfig.dna.to);

			patchName = patchName.trim();
			if(patchName.length === 0) {
				patchName = 'no name';
			}
			return {name : patchName, dna : dna, data : data};
		},
		setParserConfig : function(midiData) {
			var firstDataByte;
			if(typeof midiData[5] !== 'undefined') {
	            switch(midiData[5]) {
		            case 0x02: //single program
		            	firstDataByte = 8;
		            	break;
		            case 0x03: //edit buffer
		            	firstDataByte = 6;
		            	break;
	            }

	        	this.params.parserConfig.name.from = firstDataByte;
	        	this.params.parserConfig.name.to = this.params.parserConfig.name.from + 127;
	        	this.params.parserConfig.rawData.from = firstDataByte;
				this.params.parserConfig.dna.from = firstDataByte;
				this.params.parserConfig.rawData.to = this.params.parserConfig.dna.to = (midiData.length - 2);
			}
			return this.params.parserConfig;
		},
		convertToMsbLsbHex : function(nameArray) {
			var msbLsb = [];
			var noMSB, noLSB, valMSB, valLSB;

			for(var k=0; k<this.getMaxNameLength(); k += 1) {

				noMSB = 0;
				noLSB = k+1;
				valMSB = 0;

				if(typeof nameArray[k] !== 'undefined') {
					valLSB = nameArray[k];
				} else {
					valLSB = 0;
				}

				msbLsb.push(noMSB);
				msbLsb.push(noLSB);
				msbLsb.push(valMSB);
				msbLsb.push(valLSB);
			}
			return msbLsb;
		},
		getRequestType : function(midiData) {
			var request = false;
			if(typeof midiData[5] !== 'undefined') {
				switch(midiData[5]) {
				case 0x02:
					request = 'Program data';
					break;
				case 0x03:
					request = 'Edit buffer data';
					break;
				case 0x05:
					request = 'Request program transmit';
					break;
				case 0x06:
					request = 'Request edit buffer program transmit';
					break;
				}
			}
			return request;
		}
	};
	return obj;
});
