angular.module('f0f7.devices').factory('OberheimOb8', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'OberheimOb8',
		manuf 		: 'Oberheim',
		model 		: 'OB-8',
		hints 		: [
			'Enter <strong>Page 2</strong> mode and activate <strong>Group A</strong> button to enable <em>program dump</em>.',
			'In <strong>Page 2</strong> mode hold <strong>WRITE</strong> to dump the current STORED program.',
			'Or just click <strong>Request Programs</strong> to receive all programs.',
			'It\'s not possible to send programs to the edit buffer of OB-8',
			'Data from an OB-8 retrofitted with an Encore interface currently can only be received but not sent.'
		],
		params : {
			banks : {
				bank1 : {label : 'OB-8 Bank', address : 0x00},
			},
			patchesPerBank : 120,
			transferPause : 500,
			parserConfig : {
				dna : {
					from : 5,
					to : 59
				}
			}
		},
		sendToBuffer : function(program) {
			//Does not work on OB-8?
			var midiData = Sysex.objectToArray(program.data);

			Sysex.dumpData(midiData, 16);

			// midiData[3] = 0x0D; //Single Patch Data to Edit Buffer
			// midiData[4] = 0x00;

			// Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[3] = 0x01; //Command Byte 1: Transmit a Single Patch
			midiData[4] = prgNo; //Command Byte 2: Patch Number to transmit

			return midiData;
		},
		retrieveBank : function(bank) {		
			var header = [0xF0, 0x10, 0x01];
			bank = 0x00; // this is a fake bank. the "program data dump request" value
			Sysex.requestAllProgramsOfBank(bank, WebMIDI.midiOut, header, this.params.patchesPerBank, this.params.transferPause);
		},
		extractPatchNames : function(midiData) {
			var headerEncoreInterface = [0xF0, 0x00, 0x00, 0x2F, 0x04];
			var headerOberheimInterface = [0xF0, 0x10, 0x01];

			var data = Sysex.objectToArray(midiData);
			var receivedHeader = data.slice(0, 5);

			if(angular.equals(receivedHeader, headerEncoreInterface)) {
				console.log("Data includes headerdata of Encore MIDI interface.");
				data.splice(0,5);
				data = headerOberheimInterface.concat(data);
				// console.log("Normalized OB8 data", data);
			}
			var dna = Sysex.createDNA(data, this.params.parserConfig.dna.from, this.params.parserConfig.dna.to);
			return [{name : 'OB-8 preset', dna : dna, data : data}];
		}
	};
	return obj;
});
