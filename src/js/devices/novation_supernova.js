angular.module('f0f7.devices').factory('NovationSupernova', function($log, Sysex, $timeout, WebMIDI) {
	var obj = {
		id 			: 'NovationSupernova',
		manuf 		: 'Novation',
		model 		: 'Supernova [experimental]',
		hints 		: [
			// 'Might work with Supernova MK1, Nova Laptop, Supernova MK2 Keyboard, Nova MK2 Keyboard, Supernova MK2 Rack',
			'Might work with Supernova and Nova',
			'EXPERIMENTAL: might not work at all',
		],
		params : {
			modelId : 0x20, // Model ID (20 = SN MK1, 21 = Nova Laptop, 22 = SN II KBD, 23 = NOVA II KBD, 24 = SN II Rack)
			banks : {
				bank1 : {label : 'Bank A', address : 0x05},
				bank2 : {label : 'Bank B', address : 0x06},
				bank3 : {label : 'Bank C', address : 0x07},
				bank4 : {label : 'Bank D', address : 0x08},
			},
			patchesPerBank : 128,
			transferPause : 150,
			parserConfig : {
				name : {
					from : 10,
					to : 25
				},
				dna : {
					from : 10,
					to : 284 //296 - 9 - 3
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			// midiData[5] = this.params.modelId;
			midiData[6] = 0x7F; //Universal channel
			midiData[7] = 0x00; //Buffermode
			// midiData[8] = 0x08; //PROGRAM MODE buffer
			// midiData[8] = 0x00; //SNX - PERFORMANCE MODE Part 1 buffer?
			midiData[8] = 0x09; //If the SuperNova / Nova is in PROGRAM MODE, place the Program in the ROGRAM MODE buffer, else place the program in the PERFORMANCE MODE currently selected Part.

			midiData.splice(9, 1);	// Unset Prg
			midiData = Sysex.convertToUint8(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);
			midiData[8] = bank;
			midiData[9] = prgNo; //Prg No
			return midiData;
		},
		retrieveBank : function(bank) {
			// var midiData = [0xF0, 0x00, 0x20, 0x29, 0x01, 0x20, 0x7F, 0x13, bank, 0x00, 0xF7];
			// Sysex.sendSysex(midiData, WebMIDI.midiOut);

			var header = [0xF0, 0x00, 0x20, 0x29, 0x01, this.params.modelId, 0x7F, 0x13, bank];
			this.requestAllProgramsOfBank(header, this.params.patchesPerBank, this.params.transferPause);
		},
		requestAllProgramsOfBank : function(header, patchesPerBank, transferPause) {
			var dumpRequest = [];
			var prgNo = 0;

			var delayedDumpRequest = function(prgNo) {
				$log.debug("Requesting Prg " + prgNo);
				dumpRequest = header.concat([prgNo, 0xF7]);
				Sysex.sendSysex(dumpRequest, WebMIDI.midiOut);

				if(prgNo < (patchesPerBank-1)) {
					prgNo++;
					$timeout(delayedDumpRequest, transferPause, true, prgNo);
				} else {
					$log.info("Bank dumpRequest completed");
				}
			};
			delayedDumpRequest(prgNo);
		},
		extractPatchNames : function(midiData) {
			if ((midiData.length == 297) && (midiData[7] == 0x02)) { //Program
				return [Sysex.extractPatchName(midiData, this.params.parserConfig)];
			} else {
				return [];
			}
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return ?; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var midiData = Sysex.objectToArray(program.data);
			return Sysex.renameProgram(newName, nameLength, midiData, this.params.parserConfig);
		},
		// isOwnSysex: function(message) {
		//     return (message[1] === 0x00) &&
		//         (message[2] === 0x20) &&
		//         (message[3] === 0x29) &&
		//         (message[4] === 0x01) &&
		//         (
		//         	(message[5] === 0x20) || //SN MK1
		//         	(message[5] === 0x21) || //Nova Laptop
		//         	(message[5] === 0x22) || //SN II KBD
		//         	(message[5] === 0x23) || //NOVA II KBD
		//         	(message[5] === 0x24) //SN II Rack
		//         );
		// }
	};
	return obj;
});
