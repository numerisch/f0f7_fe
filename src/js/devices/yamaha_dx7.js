// For those interested in unpacking the uscd.edu DX7 patch data, here is
// DX7 data format information.

//      compiled from - the DX7 MIDI Data Format Sheet
//                    - article by Steve DeFuria (Keyboard Jan 87)
//                    - looking at what my DX7 spits out

// I have kept the kinda weird notation used in the DX7 Data Sheet to reduce
// typing errors. Where it doesn't quite make sense to me I've added comments.
// (And I will not be liable for errors etc ....)

// Contents: A: SYSEX Message: Bulk Data for 1 Voice
//           B: SYSEX Message: Bulk Data for 32 Voices
//           C: SYSEX Message: Parameter Change
//           D: Data Structure: Single Voice Dump & Voice Parameter #'s
//           E: Function Parameter #'s
//           F: Data Structure: Bulk Dump Packed Format

// ////////////////////////////////////////////////////////////
// A:
// SYSEX Message: Bulk Data for 1 Voice
// ------------------------------------
//        bits    hex  description

//      11110000  F0   Status byte - start sysex
//      0iiiiiii  43   ID # (i=67; Yamaha)
//      0sssnnnn  00   Sub-status (s=0) & channel number (n=0; ch 1)
//      0fffffff  00   format number (f=0; 1 voice)
//      0bbbbbbb  01   byte count MS byte
//      0bbbbbbb  1B   byte count LS byte (b=155; 1 voice)
//      0ddddddd  **   data byte 1

//         |       |       |

//      0ddddddd  **   data byte 155
//      0eeeeeee  **   checksum (masked 2's complement of sum of 155 bytes)
//      11110111  F7   Status - end sysex



// ///////////////////////////////////////////////////////////
// B:
// SYSEX Message: Bulk Data for 32 Voices
// --------------------------------------
//        bits    hex  description

//      11110000  F0   Status byte - start sysex
//      0iiiiiii  43   ID # (i=67; Yamaha)
//      0sssnnnn  00   Sub-status (s=0) & channel number (n=0; ch 1)
//      0fffffff  09   format number (f=9; 32 voices)
//      0bbbbbbb  20   byte count MS byte
//      0bbbbbbb  00   byte count LS byte (b=4096; 32 voices)
//      0ddddddd  **   data byte 1

//         |       |       |

//      0ddddddd  **   data byte 4096  (there are 128 bytes / voice)
//      0eeeeeee  **   checksum (masked 2's comp. of sum of 4096 bytes)
//      11110111  F7   Status - end sysex


// /////////////////////////////////////////////////////////////
// C:
// SYSEX MESSAGE: Parameter Change
// -------------------------------
//        bits    hex  description

//      11110000  F0   Status byte - start sysex
//      0iiiiiii  43   ID # (i=67; Yamaha)
//      0sssnnnn  10   Sub-status (s=1) & channel number (n=0; ch 1)
//      0gggggpp  **   parameter group # (g=0; voice, g=2; function)
//      0ppppppp  **   parameter # (these are listed in next section)
//                      Note that voice parameter #'s can go over 128 so
//                      the pp bits in the group byte are either 00 for
//                      par# 0-127 or 01 for par# 128-155. In the latter case
//                      you add 128 to the 0ppppppp byte to compute par#. 
//      0ddddddd  **   data byte
//      11110111  F7   Status - end sysex


// //////////////////////////////////////////////////////////////

// D:
// Data Structure: Single Voice Dump & Parameter #'s (single voice format, g=0)
// -------------------------------------------------------------------------

// Parameter
//  Number    Parameter                  Value Range
// ---------  ---------                  -----------
//   0        OP6 EG rate 1              0-99
//   1         "  "  rate 2               "
//   2         "  "  rate 3               "
//   3         "  "  rate 4               "
//   4         "  " level 1               "
//   5         "  " level 2               "
//   6         "  " level 3               "
//   7         "  " level 4               "
//   8        OP6 KBD LEV SCL BRK PT      "        C3= $27
//   9         "   "   "   "  LFT DEPTH   "
//  10         "   "   "   "  RHT DEPTH   "
//  11         "   "   "   "  LFT CURVE  0-3       0=-LIN, -EXP, +EXP, +LIN
//  12         "   "   "   "  RHT CURVE   "            "    "    "    "  
//  13        OP6 KBD RATE SCALING       0-7
//  14        OP6 AMP MOD SENSITIVITY    0-3
//  15        OP6 KEY VEL SENSITIVITY    0-7
//  16        OP6 OPERATOR OUTPUT LEVEL  0-99
//  17        OP6 OSC MODE (fixed/ratio) 0-1        0=ratio
//  18        OP6 OSC FREQ COARSE        0-31
//  19        OP6 OSC FREQ FINE          0-99
//  20        OP6 OSC DETUNE             0-14       0: det=-7
//  21 \
//   |  > repeat above for OSC 5, OSC 4,  ... OSC 1
// 125 /
// 126        PITCH EG RATE 1            0-99
// 127          "    " RATE 2              "
// 128          "    " RATE 3              "
// 129          "    " RATE 4              "
// 130          "    " LEVEL 1             "
// 131          "    " LEVEL 2             "
// 132          "    " LEVEL 3             "
// 133          "    " LEVEL 4             "
// 134        ALGORITHM #                 0-31
// 135        FEEDBACK                    0-7
// 136        OSCILLATOR SYNC             0-1
// 137        LFO SPEED                   0-99
// 138         "  DELAY                    "
// 139         "  PITCH MOD DEPTH          "
// 140         "  AMP   MOD DEPTH          "
// 141        LFO SYNC                    0-1
// 142         "  WAVEFORM                0-5, (data sheet claims 9-4 ?!?)
//                                        0:TR, 1:SD, 2:SU, 3:SQ, 4:SI, 5:SH
// 143        PITCH MOD SENSITIVITY       0-7
// 144        TRANSPOSE                   0-48   12 = C2
// 145        VOICE NAME CHAR 1           ASCII
// 146        VOICE NAME CHAR 2           ASCII
// 147        VOICE NAME CHAR 3           ASCII
// 148        VOICE NAME CHAR 4           ASCII
// 149        VOICE NAME CHAR 5           ASCII
// 150        VOICE NAME CHAR 6           ASCII
// 151        VOICE NAME CHAR 7           ASCII
// 152        VOICE NAME CHAR 8           ASCII
// 153        VOICE NAME CHAR 9           ASCII
// 154        VOICE NAME CHAR 10          ASCII
// 155        OPERATOR ON/OFF
//               bit6 = 0 / bit 5: OP1 / ... / bit 0: OP6

// Note that there are actually 156 parameters listed here, one more than in 
// a single voice dump. The OPERATOR ON/OFF parameter is not stored with the
// voice, and is only transmitted or received while editing a voice. So it
// only shows up in parameter change SYS-EX's.


// ////////////////////////////////////////////////////////

// E:
// Function Parameters: (g=2)
// -------------------------

// Parameter
// Number        Parameter           Range
// ---------     ----------          ------
// 64         MONO/POLY MODE CHANGE  0-1      O=POLY
// 65         PITCH BEND RANGE       0-12
// 66           "    "   STEP        0-12
// 67         PORTAMENTO MODE        0-1      0=RETAIN 1=FOLLOW
// 68              "     GLISS       0-1
// 69              "     TIME        0-99
// 70         MOD WHEEL RANGE        0-99
// 71          "    "   ASSIGN       0-7     b0: pitch,  b1:amp, b2: EG bias
// 72         FOOT CONTROL RANGE     0-99
// 73          "     "     ASSIGN    0-7           "
// 74         BREATH CONT RANGE      0-99
// 75           "     "   ASSIGN     0-7           "
// 76         AFTERTOUCH RANGE       0-99
// 77             "      ASSIGN      0-7           "

// ///////////////////////////////////////////////////////////////

// F:
// Data Structure: Bulk Dump Packed Format
// ---------------------------------------

// OK, now the tricky bit. For a bulk dump the 155 voice parameters for each
//  voice are packed into 32 consecutive 128 byte chunks as follows ...

// byte             bit #
//  #     6   5   4   3   2   1   0   param A       range  param B       range
// ----  --- --- --- --- --- --- ---  ------------  -----  ------------  -----
//   0                R1              OP6 EG R1      0-99
//   1                R2              OP6 EG R2      0-99
//   2                R3              OP6 EG R3      0-99
//   3                R4              OP6 EG R4      0-99
//   4                L1              OP6 EG L1      0-99
//   5                L2              OP6 EG L2      0-99
//   6                L3              OP6 EG L3      0-99
//   7                L4              OP6 EG L4      0-99
//   8                BP              LEV SCL BRK PT 0-99
//   9                LD              SCL LEFT DEPTH 0-99
//  10                RD              SCL RGHT DEPTH 0-99
//  11    0   0   0 |  RC   |   LC  | SCL LEFT CURVE 0-3   SCL RGHT CURVE 0-3
//  12  |      DET      |     RS    | OSC DETUNE     0-14  OSC RATE SCALE 0-7
//  13    0   0 |    KVS    |  AMS  | KEY VEL SENS   0-7   AMP MOD SENS   0-3
//  14                OL              OP6 OUTPUT LEV 0-99
//  15    0 |         FC        | M | FREQ COARSE    0-31  OSC MODE       0-1
//  16                FF              FREQ FINE      0-99
//  17 \
//   |  > these 17 bytes for OSC 5
//  33 /
//  34 \
//   |  > these 17 bytes for OSC 4
//  50 /
//  51 \
//   |  > these 17 bytes for OSC 3
//  67 /
//  68 \
//   |  > these 17 bytes for OSC 2
//  84 /
//  85 \
//   |  > these 17 bytes for OSC 1
// 101 /

// byte             bit #
//  #     6   5   4   3   2   1   0   param A       range  param B       range
// ----  --- --- --- --- --- --- ---  ------------  -----  ------------  -----
// 102               PR1              PITCH EG R1   0-99
// 103               PR2              PITCH EG R2   0-99
// 104               PR3              PITCH EG R3   0-99
// 105               PR4              PITCH EG R4   0-99
// 106               PL1              PITCH EG L1   0-99
// 107               PL2              PITCH EG L2   0-99
// 108               PL3              PITCH EG L3   0-99
// 109               PL4              PITCH EG L4   0-99
// 110    0   0 |        ALG        | ALGORITHM     0-31
// 111    0   0   0 |OKS|    FB     | OSC KEY SYNC  0-1    FEEDBACK      0-7
// 112               LFS              LFO SPEED     0-99
// 113               LFD              LFO DELAY     0-99
// 114               LPMD             LF PT MOD DEP 0-99
// 115               LAMD             LF AM MOD DEP 0-99
// 116  |  LPMS |      LFW      |LKS| LF PT MOD SNS 0-7   WAVE 0-5,  SYNC 0-1
// 117              TRNSP             TRANSPOSE     0-48
// 118          NAME CHAR 1           VOICE NAME 1  ASCII
// 119          NAME CHAR 2           VOICE NAME 2  ASCII
// 120          NAME CHAR 3           VOICE NAME 3  ASCII
// 121          NAME CHAR 4           VOICE NAME 4  ASCII
// 122          NAME CHAR 5           VOICE NAME 5  ASCII
// 123          NAME CHAR 6           VOICE NAME 6  ASCII
// 124          NAME CHAR 7           VOICE NAME 7  ASCII
// 125          NAME CHAR 8           VOICE NAME 8  ASCII
// 126          NAME CHAR 9           VOICE NAME 9  ASCII
// 127          NAME CHAR 10          VOICE NAME 10 ASCII

// /////////////////////////////////////////////////////////////////////


angular.module('f0f7.devices').factory('YamahaDx7', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'YamahaDx7',
		manuf 		: 'Yamaha',
		model 		: 'DX7',
		hints 		: [
			'Also works with <strong>TX802</strong> and might work with <strong>DX7II</strong>, <strong>TX7</strong> and <strong>TX816</strong>',
			'Compatible with <strong>Dexed</strong> banks',
            'Button <strong>FUNCTION</strong> then <strong>button 8</strong>: set channel number <strong>MIDI CH=1</strong>',
            'Button 8 again: set <strong>SYS INFO AVAIL</strong> with button <strong>YES</strong>',
            'Button <strong>INTERNAL MEMORY PROTECT</strong>: set to <strong>OFF</strong>',
			'You have to add at least <strong>32</strong> programs to a bank or <strong>Overwrite Bank</strong> will not work',
            'You cannot request programs from <strong>supermaxed</strong> DX7, on those devices press 3 times button 8 to execute <strong>MIDI TRANSMIT</strong>'
		],
		params : {
			deviceId	: 0x10, //DeviceId omni
			singleProgramsCannotBeTransmitted : true,
			banks : {
				bank1 : {label : 'Dx7 Bank', address : 0x00},
			},
			patchesPerBank : 32,
			transferPause : 5,
			buffer 		: [],
			parserConfig : {
				name : {
					from : 118,
					to : 127
					// from : 145,
					// to : 154
				},
				dna : {
					from : 0,
					to : 127
				}
			}
		},
		sendToBuffer : function(program, outputPort) {		
			var header = [0xF0, 0x43, 0x00, 0x00, 0x01, 0x1B];
			var rawData = Sysex.objectToArray(program.data);
			var expandedData = this.expand(rawData);
			var checksum = Sysex.calculateRolandChecksum(expandedData);
			var eox = [checksum, 0xF7];
			var midiData = header.concat(expandedData, eox);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			if(midiData.length > 0) {
				Sysex.sendSysex(midiData, WebMIDI.midiOut);
			}
		},
		reorderProgram : function(program, bank, prgNo) {
			if(prgNo === 0) {
				this.params.buffer = [];
			}
			var rawData = Sysex.objectToArray(program.data);
			this.params.buffer = this.params.buffer.concat(rawData);

			if(prgNo == (this.params.patchesPerBank-1)) {
				var header = [0xF0, 0x43, 0x00, 0x09, 0x20, 0x00];
				var checksum = Sysex.calculateRolandChecksum(this.params.buffer);
				var eox = [checksum, 0xF7];
				var midiData = header.concat(this.params.buffer, eox);
				this.params.buffer = [];
				return midiData;
			}
			return [];
		},
		retrieveBank : function(bank) {
			//F0 43 20 09 F7
			var midiData = [0xF0, 0x43, 0x20, 0x09, 0xF7];
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		extractPatchNames : function(midiData) {
			var program = [];

			midiData = Sysex.objectToArray(midiData);

			// switch(midiData[3]) {
			// 	case 0x00 : program = this.extractSinglePatch(midiData); break; //format number (f=0; 1 voice)
			// 	case 0x09 : program = this.extractMultiDump(midiData); break; //format number (f=9; 32 voices)
			// }
			program = this.extractMultiDump(midiData); //always extract Multidump to be compatible with Dexed
			return program;
		},
		extractSinglePatch : function(midiData) {
			// midiData = midiData.slice(6, (midiData.length-2));
			// return [Sysex.extractPatchName(midiData, this.params.parserConfig)];
			return [];
		},
		extractMultiDump : function(midiData) {
			midiData = midiData.slice(6, (midiData.length-2));

			var chunk = 128;
			var i,j,data;
			var patchNames = [];

			for (i=0,j=midiData.length; i<j; i+=chunk) {
				data = midiData.slice(i,i+chunk);
				Sysex.dumpData(data, 16);
				patchNames.push(Sysex.extractPatchName(data, this.params.parserConfig));
			}
			return patchNames;
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 10; 
		},
		renameProgram : function(newName, program) {
			// newName = newName.toUpperCase();
			var nameLength = this.getMaxNameLength();
			var midiData = Sysex.objectToArray(program.data);
			return Sysex.renameProgram(newName, nameLength, midiData, this.params.parserConfig);
		},
		expand : function(midiData) {
			var expandedData = [];
			var oscData = midiData.concat([]); //Copy Array
			for(var osc=6; osc>=1; osc--) {
				var packedOscData = oscData.splice(0, 17);
				for(var i=0; i<11; i++) {
					expandedData.push(packedOscData[i]);
				}
				expandedData.push((packedOscData[11] & 0x03)); //LFT CURVE
				expandedData.push(((packedOscData[11] >> 2) & 0x03)); //RHT CURVE
				expandedData.push((packedOscData[12] & 0x07)); //OSC RATE SCALE 0-7
				expandedData.push((packedOscData[13] & 0x03)); //AMP MOD SENS   0-3
				expandedData.push(((packedOscData[13] >> 2) & 0x07)); // KEY VEL SENS   0-7 
				expandedData.push(packedOscData[14]); // OPERATOR OUTPUT LEVEL
				expandedData.push(packedOscData[15] & 0x01); // OSC MODE (fixed/ratio) 0-1 
				expandedData.push((packedOscData[15] >> 1) & 0x1F); // OSC FREQ COARSE        0-31
				expandedData.push(packedOscData[16]); // OSC FREQ FINE          0-99
				expandedData.push((packedOscData[12] >> 3) & 0x0F); // OSC DETUNE             0-14       0: det=-7
			}
			expandedData.push(midiData[102]);
			expandedData.push(midiData[103]);
			expandedData.push(midiData[104]);
			expandedData.push(midiData[105]);
			expandedData.push(midiData[106]);
			expandedData.push(midiData[107]);
			expandedData.push(midiData[108]);
			expandedData.push(midiData[109]);
			expandedData.push(midiData[110] & 0x1F); //134 ALGORITHM 0-31
			expandedData.push(midiData[111] & 0x07); //135 FEEDBACK 0-7
			expandedData.push((midiData[111] >> 3) & 0x01); //136 OSCILLATOR SYNC 0-1
			expandedData.push(midiData[112]); //137 LFO SPEED 0-99
			expandedData.push(midiData[113]); //DELAY
			expandedData.push(midiData[114]); //PITCH MOD DEPTH   
			expandedData.push(midiData[115]); //AMP   MOD DEPTH  
			expandedData.push(midiData[116] & 0x01); //141 LFO SYNC 0-1
			expandedData.push((midiData[116] >> 1 )& 0x0F); //142 WAVEFORM 0-5
			expandedData.push((midiData[116] >> 5 )& 0x03); //143 PITCH MOD SENSITIVITY 0-7
			expandedData.push(midiData[117]); //TRANSPOSE 0-48
			expandedData.push(midiData[118]); //ASCII
			expandedData.push(midiData[119]); //ASCII
			expandedData.push(midiData[120]); //ASCII
			expandedData.push(midiData[121]); //ASCII
			expandedData.push(midiData[122]); //ASCII
			expandedData.push(midiData[123]); //ASCII
			expandedData.push(midiData[124]); //ASCII
			expandedData.push(midiData[125]); //ASCII
			expandedData.push(midiData[126]); //ASCII
			expandedData.push(midiData[127]); //ASCII
			return expandedData;
		},
		pack : function(midiData) {

		}
	};
	return obj;
});
