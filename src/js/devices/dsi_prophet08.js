angular.module('f0f7.devices').factory('DSIProphet08', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'DSIProphet08',
		manuf 		: 'Dave Smith Instruments',
		model 		: 'Prophet 08',
		hints 		: [
			'In <strong>Global</strong> menu set <strong>MIDI SysEx</strong> to <strong>on</strong>',
		],
		params : {
			banks : {
				bank1 : {label : 'Prophet 08 / Bank 1', address : 0x00},
				bank2 : {label : 'Prophet 08 / Bank 2', address : 0x01},
			},
			patchesPerBank : 128,
			transferPause : 300,
			parserConfig : {
				rawData : {
					from: 6,
					to: 445 //439 bytes total raw data, 384 unpacked
				},
				name : {
					from : 184,
					to : 199
				},
				dna : {
					from : 0,
					to : 384
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			midiData = Array.from(midiData);
			midiData[3] = 0x03; 	// Edit Buffer Data
			midiData.splice(4, 2);	// Unset Bank and Prg
			midiData = Sysex.convertToUint8(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[3] = 0x02; 	// Program Data Dump
			midiData[4] = bank;		// 0x00 or 0x01
			midiData[5] = prgNo;	// 0-127

			return midiData;
		},
		retrieveBank : function(bank) {
			var header = [0xF0, 0x01, 0x23, 0x05];
			Sysex.requestAllProgramsOfBank(bank, WebMIDI.midiOut, header, this.params.patchesPerBank, this.params.transferPause);
		},
		extractPatchNames : function(midiData) {
			midiData = Sysex.objectToArray(midiData);
			var unpackedData = this.unpackMidiData(midiData);
			var program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			program.data = midiData;
			return [program];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 16; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var newNameArray = Sysex.createPaddedNameArray(nameLength, newName);

			var midiData = Sysex.objectToArray(program.data);
			var header = midiData.slice(0, this.params.parserConfig.rawData.from);
			var unpackedData = this.unpackMidiData(midiData);

			unpackedData = Sysex.changeNameinMidiData(newNameArray, nameLength, unpackedData, this.params.parserConfig);
			program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			
			var packedData = Sysex.packDSISysex(unpackedData);
			program.data = header.concat(packedData, 0xF7);
			return program;
		},
		unpackMidiData : function(midiData) {
			var packedData = midiData.slice(this.params.parserConfig.rawData.from, this.params.parserConfig.rawData.to);
			var unpackedData = Sysex.unpackDSISysex(packedData);
			// Sysex.dumpData(unpackedData, 16);
			return unpackedData;
		}
	};
	return obj;
});
