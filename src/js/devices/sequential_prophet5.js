angular.module('f0f7.devices').factory('SequentialProphet5', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'SequentialProphet5',
		manuf 		: 'Sequential',
		model 		: 'Prophet 5 / 10 Rev4',
		hints 		: [
			'In <strong>GLOBALS</strong> menu set <strong>MIDI SysEx</strong> to <strong>Midi</strong> or <strong>USB</strong> depending on which connection you use.',
            'The <strong>FACTORY BANK GROUPS</strong> are meant to be *read only*. However LASER Mammoth lets you overwrite them too. Do this only if you really need to.',
			'<strong>Caution:</strong> when moving stacked or splitted programs the reference to the <em>Layer B</em> program will not be updated.',
		],
		params : {
			banks : {
				bank1 : {label : 'P~5 / User Bank Group 1', address : 0x00},
                bank2 : {label : 'P~5 / User Bank Group 2', address : 0x01},
                bank3 : {label : 'P~5 / User Bank Group 3', address : 0x02},
                bank4 : {label : 'P~5 / User Bank Group 4', address : 0x03},
                bank5 : {label : 'P~5 / User Bank Group 5', address : 0x04},
                bank6 : {label : 'P~5 / Factory Bank Group 1', address : 0x05},
                bank7 : {label : 'P~5 / Factory Bank Group 2', address : 0x06},
                bank8 : {label : 'P~5 / Factory Bank Group 3', address : 0x07},
                bank9 : {label : 'P~5 / Factory Bank Group 4', address : 0x08},
                bank10 : {label : 'P~5 / Factory Bank Group 5', address : 0x09},
			},
			patchesPerBank : 40,
			transferPause : 80,
			parserConfig : {
				rawData : {
					from: 6,
                    to: 158 //152 bytes packed, 128 expanded
				},
				name : {
					from : 65,
					to : 84
				},
				dna : {
					from : 0,
					to : 128
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			midiData = Array.from(midiData);
			midiData[3] = 0x03; 	// Edit Buffer Data
			midiData.splice(4, 2);	// Unset Bank and Prg
			midiData = Sysex.convertToUint8(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[3] = 0x02; 	// Program Data Dump
			midiData[4] = bank;		// Group 0 - 9
			midiData[5] = prgNo;	// Program 0 - 39

			return midiData;
		},
		retrieveBank : function(bank) {
			var header = [0xF0, 0x01, 0x32, 0x05];
			Sysex.requestAllProgramsOfBank(bank, WebMIDI.midiOut, header, this.params.patchesPerBank, this.params.transferPause);
		},
		extractPatchNames : function(midiData) {
			midiData = Sysex.objectToArray(midiData);
			var unpackedData = this.unpackMidiData(midiData);
			var program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			program.data = midiData;
			return [program];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 22; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var newNameArray = Sysex.createPaddedNameArray(nameLength, newName);

			var midiData = Sysex.objectToArray(program.data);
			var header = midiData.slice(0, this.params.parserConfig.rawData.from);
			var unpackedData = this.unpackMidiData(midiData);

			unpackedData = Sysex.changeNameinMidiData(newNameArray, nameLength, unpackedData, this.params.parserConfig);
			program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			
			var packedData = Sysex.packDSISysex(unpackedData);
			program.data = header.concat(packedData, 0xF7);
			return program;
		},
		unpackMidiData : function(midiData) {
			var packedData = midiData.slice(this.params.parserConfig.rawData.from, this.params.parserConfig.rawData.to);
			var unpackedData = Sysex.unpackDSISysex(packedData);
			// Sysex.dumpData(unpackedData, 16);
			return unpackedData;
		}
	};
	return obj;
});
