angular.module('f0f7.devices').factory('DSIMophoX4', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'DSIMophoX4',
		manuf 		: 'Dave Smith Instruments',
		model 		: 'Mopho x4 [beta]',
		hints 		: [
			'In <strong>Global</strong> menu set <strong>MIDI SysEx</strong> to <strong>on</strong>',
			'To edit the Global parameters, hold down the PROGRAM MODE switch until Global Parameter is displayed.'
		],
		params : {
			banks : {
				bank1 : {label : 'Mopho x4 / Bank 1', address : 0x00},
				bank2 : {label : 'Mopho x4 / Bank 2', address : 0x01},
				bank3 : {label : 'Mopho x4 / Bank 3', address : 0x02},
                bank4 : {label : 'Mopho x4 / Bank 4', address : 0x03},
			},
			patchesPerBank : 128,
			transferPause : 300,
			parserConfig : {
				rawData : {
					from: 6,
					to: 299 //256 bytes expanded to 293 MIDI bytes in “packed MS bit” format
				},
				name : {
					from : 184,
					to : 199
				},
				dna : {
					from : 0,
					to : 256
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			midiData = Array.from(midiData);
			midiData[3] = 0x03; 	// Edit Buffer Data
			midiData.splice(4, 2);	// Unset Bank and Prg
			midiData = Sysex.convertToUint8(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[3] = 0x02; 	// Program Data Dump
			midiData[4] = bank;		// 0x00, 0x01 or 0x02
			midiData[5] = prgNo;	// 0-127

			return midiData;
		},
		retrieveBank : function(bank) {
			var header = [0xF0, 0x01, 0x29, 0x05];
			Sysex.requestAllProgramsOfBank(bank, WebMIDI.midiOut, header, this.params.patchesPerBank, this.params.transferPause);
		},
		extractPatchNames : function(midiData) {
			midiData = Sysex.objectToArray(midiData);
			var unpackedData = this.unpackMidiData(midiData);
			var program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			program.data = midiData;
			return [program];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 16; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var newNameArray = Sysex.createPaddedNameArray(nameLength, newName);

			var midiData = Sysex.objectToArray(program.data);
			var header = midiData.slice(0, this.params.parserConfig.rawData.from);
			var unpackedData = this.unpackMidiData(midiData);

			unpackedData = Sysex.changeNameinMidiData(newNameArray, nameLength, unpackedData, this.params.parserConfig);
			program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			
			var packedData = Sysex.packDSISysex(unpackedData);
			program.data = header.concat(packedData, 0xF7);
			return program;
		},
		unpackMidiData : function(midiData) {
			var packedData = midiData.slice(this.params.parserConfig.rawData.from, this.params.parserConfig.rawData.to);
			var unpackedData = Sysex.unpackDSISysex(packedData);
			// Sysex.dumpData(unpackedData, 16);
			return unpackedData;
		}
	};
	return obj;
});
