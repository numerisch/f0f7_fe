angular.module('f0f7.devices').factory('SequentialTake5', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'SequentialTake5',
		manuf 		: 'Sequential',
		model 		: 'Take 5',
		hints 		: [
			'In <strong>GLOBALS</strong> menu set <strong>MIDI SysEx On</strong> to <strong>On</strong>.',
			'In <strong>GLOBALS</strong> menu set <strong>MIDI SysEx Cable</strong> to <strong>Midi</strong> or <strong>USB</strong> depending on which connection you use.',
		],
		params : {
			banks : {
				bank1 : {label : 'User Bank 1', address : 0x00},
				bank2 : {label : 'User Bank 2', address : 0x01},
				bank3 : {label : 'User Bank 3', address : 0x02},
				bank4 : {label : 'User Bank 4', address : 0x03},
				bank5 : {label : 'User Bank 5', address : 0x04},
				bank6 : {label : 'User Bank 6', address : 0x05},
				bank7 : {label : 'User Bank 7', address : 0x06},
				bank8 : {label : 'User Bank 8', address : 0x07},
				bank9 : {label : 'User Bank 9', address : 0x08},
				bank10 : {label : 'User Bank 10', address : 0x09},
				bank11 : {label : 'User Bank 11', address : 0x0a},
				bank12 : {label : 'User Bank 12', address : 0x0b},
				bank13 : {label : 'User Bank 13', address : 0x0c},
				bank14 : {label : 'User Bank 14', address : 0x0d},
				bank15 : {label : 'User Bank 15', address : 0x0e},
				bank16 : {label : 'User Bank 16', address : 0x0f},
				bank17 : {label : 'Factory Bank 1', address : 0x10},
				bank18 : {label : 'Factory Bank 2', address : 0x11},
				bank19 : {label : 'Factory Bank 3', address : 0x12},
				bank20 : {label : 'Factory Bank 4', address : 0x13},
				bank21 : {label : 'Factory Bank 5', address : 0x14},
				bank22 : {label : 'Factory Bank 6', address : 0x15},
				bank23 : {label : 'Factory Bank 7', address : 0x16},
				bank24 : {label : 'Factory Bank 8', address : 0x17},
				bank25 : {label : 'Factory Bank 9', address : 0x18},
				bank26 : {label : 'Factory Bank 10', address : 0x19},
				bank27 : {label : 'Factory Bank 11', address : 0x1a},
				bank28 : {label : 'Factory Bank 12', address : 0x1b},
				bank29 : {label : 'Factory Bank 13', address : 0x1c},
				bank30 : {label : 'Factory Bank 14', address : 0x1d},
				bank31 : {label : 'Factory Bank 15', address : 0x1e},
				bank32 : {label : 'Factory Bank 16', address : 0x1f},
			},
			// patchesPerBank : 128,
			patchesPerBank : 16,
			transferPause : 400,
			parserConfig : {
				rawData : {
					from: 6,
                    to: 4696 //4690 bytes packed, 4096 expanded
				},
				name : {
					from : 195,
					to : 214
				},
				dna : {
					from : 0,
					to : 4096
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			midiData = Array.from(midiData);
			midiData[3] = 0x03; 	// Edit Buffer Data
			midiData.splice(4, 2);	// Unset Bank and Prg
			midiData = Sysex.convertToUint8(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[3] = 0x02; 	// Program Data Dump
			midiData[4] = bank;		// Bank 0 - 7
			midiData[5] = prgNo;	// Program 1 - 16

			return midiData;
		},
		retrieveBank : function(bank) {
			var header = [0xF0, 0x01, 0x35, 0x05];
			Sysex.requestAllProgramsOfBank(bank, WebMIDI.midiOut, header, this.params.patchesPerBank, this.params.transferPause);
		},
		extractPatchNames : function(midiData) {
			midiData = Sysex.objectToArray(midiData);
			var unpackedData = this.unpackMidiData(midiData);
			var program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			program.data = midiData;
			return [program];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1;
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var newNameArray = Sysex.createPaddedNameArray(nameLength, newName);

			var midiData = Sysex.objectToArray(program.data);
			var header = midiData.slice(0, this.params.parserConfig.rawData.from);
			var unpackedData = this.unpackMidiData(midiData);

			unpackedData = Sysex.changeNameinMidiData(newNameArray, nameLength, unpackedData, this.params.parserConfig);
			program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			
			var packedData = Sysex.packDSISysex(unpackedData);
			program.data = header.concat(packedData, 0xF7);
			return program;
		},
		unpackMidiData : function(midiData) {
			var packedData = midiData.slice(this.params.parserConfig.rawData.from, this.params.parserConfig.rawData.to);
			var unpackedData = Sysex.unpackDSISysex(packedData);
			// Sysex.dumpData(unpackedData, 16);
			return unpackedData;
		}
	};
	return obj;
});
