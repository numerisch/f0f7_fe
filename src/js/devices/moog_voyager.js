angular.module('f0f7.devices').factory('MoogVoyager', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'MoogVoyager',
		manuf 		: 'Moog',
		model 		: 'Voyager',
		hints 		: [
			'In <strong>Master</strong> menu set <strong>SysEx Device ID</strong> to <strong>1</strong>',
		],
		params : {
			deviceId	: 0x00,
			banks : {
				bank1 : {label : 'Switch Banks on the Voyager!', address : 0x00},
			},
			patchesPerBank : 128,
			transferPause : 300,
			parserConfig : {
				name : {
					from : 84,
					to : 107
				},
				dna : {
					from : 0,
					to : 128
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);
			midiData = Sysex.packMoogSysex(midiData, [0xF0, 0x04, 0x01, this.params.deviceId, 0x02]); //Voyager Panel Dump.

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);
			midiData = Sysex.packMoogSysex(midiData, [0xF0, 0x04, 0x01, this.params.deviceId, 0x03, prgNo]); //Voyager Single Preset Dump.

			return midiData;
		},
		retrieveBank : function(bank) {
			var midiData = [0xF0, 0x04, 0x01, this.params.deviceId, 0x04, 0xF7]; //All Presets Dump Request
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		extractPatchNames : function(midiData) {
			var headerLength;
			var parserConfig = this.params.parserConfig;
			if(midiData[4] == 0x03) { //Voyager Single Preset Dump. Multiple programs in dump.
				headerLength = 6;
				midiData = Sysex.unpackMoogSysex(midiData, headerLength);
				return [Sysex.extractPatchName(midiData, parserConfig)];

			} else {
				headerLength = 5;
				var ignoreLeadingBytes = 3;
				var chunk = 128;

				midiData = Sysex.unpackMoogSysex(midiData, headerLength);

				var i,j,data;
				var patchNames = [];

				for (i=ignoreLeadingBytes,j=midiData.length; i<j; i+=chunk) {
					data = midiData.slice(i,i+chunk);
					patchNames.push(Sysex.extractPatchName(data, parserConfig));
				}
				return patchNames;
			}
		},
		getDeviceId : function(midiData) {
			return midiData[3];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 24; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			return Sysex.renameProgram(newName, nameLength, program.data, this.params.parserConfig);
		}
	};
	return obj;
});
