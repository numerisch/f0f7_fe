angular.module('f0f7.devices').factory('IntellijelRainmaker', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'IntellijelRainmaker',
		manuf 		: 'Intellijel Cylonix',
		model 		: 'Rainmaker',
		hints 		: [
			'Enable <strong>USB/MIDI</strong> mode first by pushing the <strong>SAVE & LOAD</strong> buttons on the front panel simultaneously'
		],
		params : {
			banks : {
				bank1 : {label : 'Rainmaker Bank', address : 0x01},
			},
			patchesPerBank : 128,
			transferPause : 150,
			parserConfig : {
				rawData : {
					from: 6,
					to: 153
				},
				name : {
					from : 116,
					to : 126
				},
				dna : {
					from : 6,
					to : 153
				}
			}
		},
		rmCharset : [
				' ',
				'A',
				'B',
				'C',
				'D',
				'E',
				'F',
				'G',
				'H',
				'I',
				'J',
				'K',
				'L',
				'M',
				'N',
				'O',
				'P',
				'Q',
				'R',
				'S',
				'T',
				'U',
				'V',
				'W',
				'X',
				'Y',
				'Z',
				'a',
				'b',
				'c',
				'd',
				'e',
				'f',
				'g',
				'h',
				'i',
				'j',
				'k',
				'l',
				'm',
				'n',
				'o',
				'p',
				'q',
				'r',
				's',
				't',
				'u',
				'v',
				'w',
				'x',
				'y',
				'z',
				'0',
				'1',
				'2',
				'3',
				'4',
				'5',
				'6',
				'7',
				'8',
				'9',
				'!',
				'"',
				'#',
				'$',
				'%',
				'&',
				'\'',
				'(',
				')',
				'*',
				'+',
				',',
				'-',
				'.',
				'/',
				':',
				';',
				'<',
				'=',
				'>',
				'?',
				'@',
				'[',
				'\\',
				']',
				'^',
				'_',
				'`',
				'{',
				'|',
				'}',
				'~',
				' ',
		],
		sendToBuffer : function(program) {
			$log.log("not possible to send programs to the edit buffer on Rainmaker");
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			// To upload a single preset (from the computer to the Rainmaker module):
			// 0xF0, 0x00, 0x02, 0x14, 0x00, 0xNN, 0xDD ... 0xCC 0xF7
			// where 0xDD ... are the data bytes containing the preset parameter values.
			var midiData = Sysex.objectToArray(program.data);
			midiData[5] = prgNo;

			var unpackedData = this.unpackMidiData(midiData);
			midiData[midiData.length-2] = Sysex.calculateRolandChecksum(unpackedData);

			return midiData;
		},
		retrieveBank : function(bank) {
			//To download all 128 presets at once: 0xF0, 0x00, 0x02, 0x14, 0x02, 0xF7
			//There will be a series of 128 responses as when downloading a single preset. 
			var midiData = [0xF0, 0x00, 0x02, 0x14, 0x02, 0xF7];
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		extractPatchNames : function(midiData) {
			var parserConfig = this.params.parserConfig;
			midiData = Sysex.objectToArray(midiData);
			
			var unpackedData = this.unpackMidiData(midiData);
			var nameArray = unpackedData.slice(parserConfig.name.from, parserConfig.name.to);

			var patchName = this.nameArrayToString(nameArray);

			var dna = Sysex.createDNA(midiData, parserConfig.dna.from, parserConfig.dna.to);
			return [{name : patchName, dna : dna, data : midiData}];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from; //return 10; 
		},
		nameArrayToString : function(nameArray) {
			var patchName = '';
			for(var i=0; i<nameArray.length; i++) {
			  patchName += this.mapRainmakerHexToASCII(nameArray[i]);
			}

			patchName = patchName.trim();
			if(patchName.length === 0) {
				patchName = 'no name';
			}

			return patchName;
		},
		mapRainmakerHexToASCII : function(hex) {
			if(typeof this.rmCharset[hex] !== 'undefined') {
				return this.rmCharset[hex];	
			} else {
				return ' ';
			}
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var newNameArray = this.createPaddedRMArray(newName);

			var midiData = Sysex.objectToArray(program.data);
			var header = midiData.slice(0, this.params.parserConfig.rawData.from);
			var unpackedData = this.unpackMidiData(midiData);

			unpackedData = Sysex.changeNameinMidiData(newNameArray, nameLength, unpackedData, this.params.parserConfig);
			
			var packedData = Sysex.packDSISysex(unpackedData);

			var checksum = Sysex.calculateRolandChecksum(unpackedData);
			var eox = [checksum, 0xF7];

			midiData = header.concat(packedData, eox);
			program = this.extractPatchNames(midiData);

			return program[0];
		},
		unpackMidiData : function(midiData) {
			var packedData = midiData.slice(this.params.parserConfig.rawData.from, this.params.parserConfig.rawData.to);
			return Sysex.unpackDSISysex(packedData);
		},
		createPaddedRMArray : function (str) {
			var nameLength = this.getMaxNameLength();
			var bytes = [];
			var charCode, index;
			
			for (var i = 0; i < nameLength; i++) {
			  if(typeof str[i] === 'undefined') {
			  	index = 0; //index of space
			  } else {
			  	index = this.rmCharset.indexOf(str[i]);
			  }
			  bytes.push(index);
			}
		  	return bytes;
		},
	};
	return obj;
});
