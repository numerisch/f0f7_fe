angular.module('f0f7.sysexService', [])
.factory('Sysex', function($log, $timeout, md5) {
	var obj = {
		params : {
			logData		: ''
		},
		extractPatchName : function(midiData, parserConfig) {
			var data = angular.fromJson(angular.toJson(midiData));
			var patchName = '';
            var char;

			for(var k=parserConfig.name.from; k<=parserConfig.name.to; k++) {
				if(typeof data[k] !== 'undefined') {
					if(data[k] < 32 || data[k] > 127) {
						char = 32;
					} else {
                        char = data[k];
                    }
					patchName += String.fromCharCode(char);
				}
			}

			var dna = this.createDNA(midiData, parserConfig.dna.from, parserConfig.dna.to);

			patchName = patchName.trim();
			if(patchName.length === 0) {
				patchName = 'no name';
			}
			return {name : patchName, dna : dna, data : data};
		},
		createDNA : function(midiData, from, to) {
			var dnaArray = midiData.slice(from, to);
			return md5.createHash(JSON.stringify(dnaArray) || '');
		},
		unpackMoogSysex : function(midiData, headerLength) {
			var resultData = [];
			var decodedByteCount = 0;  // Number of expanded bytes in result

			var cumulator = 0;
			var bitCount = 0;
			var patchName = '';

			for (var i = headerLength; // Skip header
				i < midiData.length - 1; // Omit EOF
				i++)
			{
				cumulator |= midiData[i] << bitCount;
				bitCount += 7;
				//console.log((i - 7) + ':' + byteToHex(midiData[i]) + ' (' + bitCount + ') ' + decimalToHex(cumulator) + '\n');
				if (bitCount >= 8)
				{
					var byte = cumulator & 0xFF;
					bitCount -= 8;
					cumulator >>= 8;
					resultData[decodedByteCount++] = byte;
					// console.log((i - 7) + ':' + byteToHex(midiData[i]) + ' (' + bitCount + ') ' + decimalToHex(cumulator) + ' > '  + byteToHex(byte) + '\n');

				}
			}

			return resultData;
		},
		packMoogSysex : function(midiData, header) {
			// var header = [0xF0, 0x04, 0x01, 0x00, 0x03, 0x00]; //Voyager Single Preset Dump. Device ID!
			// var header = [0xF0, 0x04, 0x01, 0x00, 0x02]; //Voyager Panel Dump. Device ID == 0x00

			var resultData = [];
			var packedByteCount = 0;
			var bitCount = 0;

			var thisByte;
			var packedByte;
			var nextByte = 0x0;


			for (var i = 0; i <= midiData.length; i++)
			{
				thisByte = midiData[i];
				packedByte = ((thisByte << bitCount) | nextByte) & 0x7F;
				nextByte = midiData[i] >> (7-bitCount);

				resultData[packedByteCount++] = packedByte;

				bitCount++;
				if(bitCount >= 7) {
					bitCount = 0;

					//Fill last byte
					packedByte = nextByte & 0x7F;
					resultData[packedByteCount++] = packedByte;
					nextByte = 0x0;
				}
			}

			resultData[packedByteCount++] = 0xF7;
			resultData = header.concat(resultData);

			return resultData;
		},
		unpackDSISysex : function(midiData) {

			midiData = midiData.concat([]); //Copy Array

			var unpackedData = [];
			var msb;
			var daveByte;
			var packets;
			var i;

			while (midiData.length > 0) {
				daveByte = midiData.splice(0, 1);
				packets = midiData.splice(0, 7);
				for(i=0; i<packets.length; i++) {
					msb = daveByte & 0x01;
					unpackedData.push(packets[i] | (msb << 7));
					daveByte = daveByte >> 1;
				}
			}
			return unpackedData;
		},
		packDSISysex : function(midiData) {

			midiData = midiData.concat([]); //Copy Array
			
			var packedData = [];
			var msb;
			var daveByte;
			var inputData;
			var i;

			while (midiData.length > 0) {
				inputData = midiData.splice(0, 7);
				daveByte = 0x00;
				for(i=0; i<inputData.length; i++) {
					msb = inputData[i] & 0x80;
					daveByte = daveByte | (msb >> (7-i));
				}
				packedData.push(daveByte);
				for(i=0; i<inputData.length; i++) {
					packedData.push(inputData[i] & 0x7F);
				}
			}
			return packedData;
		},
		sendSysex : function(midiData, outputPort) {
			this.dumpData(midiData, 16);
			outputPort.send( midiData, 0 ); // Data, timestamp
			// outputPort.send( midiData, window.performance.now() + 1000.0 ); // Data, timestamp
		},
		requestAllProgramsOfBank : function(bank, outputPort, header, patchesPerBank, transferPause) {
			var dumpRequest = [];
			var prgNo = 0;

			var delayedDumpRequest = function(prgNo) {
				$log.debug("Requesting Prg " + prgNo);
				dumpRequest = header.concat([bank, prgNo, 0xF7]);
				obj.sendSysex(dumpRequest, outputPort);

				if(prgNo < (patchesPerBank-1)) {
					prgNo++;
					$timeout(delayedDumpRequest, transferPause, true, prgNo);
				} else {
					$log.info("Bank dumpRequest completed");
				}
			};
			delayedDumpRequest(prgNo);
		},
		calculateRolandChecksum : function(midiData) {
			var sum = 0;
			for (var i = 0; i < midiData.length; i++) {
				sum += midiData[i];
			}
			sum &= 0x7f;
			if (sum !== 0) {
				sum = 0x80 - sum;
			}
			return sum;
		},
		calculateOberheimChecksum : function(midiData) {
			var sum = 0;
			for (var i = 0; i < midiData.length; i++) {
				sum += midiData[i];
			}
			sum &= 0x7f;
			return sum;
		},
		calculateAccessChecksum : function(midiData) {
			//Checksum (DeviceID + 10 + BankNumber + ProgramNumber + [256 single bytes]) AND 7F.
			var checksum = midiData[5] + 0x10 + midiData[7] + midiData[8];
			for(var i=9; i<=(midiData.length-3); i++) {
				checksum += midiData[i];
			}
			return checksum & 0x7F;
		},
        calculateWaldorfChecksum : function(midiData) {
            // Sum of all databytes truncated to 7 bits.
            // The addition is done in 8 bit format, the result is
            // masked to 7 bits (00h to 7Fh). A checksum of 7Fh is
            // always accepted as valid.
            // IMPORTANT: the MIDI status-bytes as well as the
            // ID's are not used for computing the checksum.

            var checksum = 0x00;
            for(var i=0; i<(midiData.length); i++) {
                checksum += midiData[i];
            }
            return checksum & 0x7F;
        },
		calculateStrymonChecksum : function(midiData) {
			var checksum = 0x00;
			for(var i=9; i<(midiData.length-2); i++) {
				checksum += 0x7F & midiData[i];
			}
			return 0x7F&checksum;
		},
		dumpData : function(data, lineLength) {
			this.appendToScreen("Found " + data.length + " bytes");

			var txt = '';
			for (var i = 0; i < data.length; i++) {
				var rd = data[i];
				if (rd > 31) {
					txt += String.fromCharCode(rd);
				} else {
					txt += '.';
				}

				this.appendToScreen(this.byteToHex(rd) + ' ', false);

				if ((i+1) % lineLength === 0) {
					this.appendToScreen(' ' + txt);
					txt = '';
				}
			}
			this.appendToScreen(' ' + txt);
		},
		nibbleToHex : function(halfByte) {
			return String.fromCharCode(halfByte < 10 ?
				halfByte + 48 : // 0 to 9
				halfByte + 55); // A to F
		},
		byteToHex : function(dec) {
			var h = (dec & 0xF0) >> 4;
			var l = dec & 0x0F;
			return this.nibbleToHex(h) + this.nibbleToHex(l);
		},
		decimalToHex : function(dec) {
			var result = '';
			do {
				result = this.byteToHex(dec & 0xFF) + result;
				dec >>= 8;
			} while (dec > 0);
			return result;
		},
		appendToScreen : function(msg, linebreak) {
			if(typeof linebreak == 'undefined') {
				linebreak =true;
			}
			this.params.logData += msg;
			if(linebreak === true) {
				this.params.logData += "\n";
			}
		},
		convertToUint8 : function(raw) {
			var rawLength = raw.length;
			var arrayUint8 = new Uint8Array(new ArrayBuffer(rawLength));
			for(var i=0; i < rawLength; i++) {
				// arrayUint8[i] = raw.charCodeAt(i);
				arrayUint8[i] = raw[i];
			}
			return arrayUint8;
		},
		objectToArray : function(obj) {
			var array = [];
			for (var prop in obj){
				if(obj.hasOwnProperty(prop)){
					array.push(obj[prop]);
				}
			}
			return array;
		},
		decodeDataURI : function(dataURI) {
			var BASE64_MARKER = ';base64,';
			var base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
			var base64 = dataURI.substring(base64Index);
			return window.atob(base64);
		},
		renameProgram : function(newName, nameLength, midiData, parserConfig) {
			var newNameArray = this.createPaddedNameArray(nameLength, newName);
			midiData = this.changeNameinMidiData(newNameArray, nameLength, midiData, parserConfig);
			return this.extractPatchName(midiData, parserConfig);
		},
		changeNameinMidiData : function(newNameArray, nameLength, midiData, parserConfig) {
			var args = [parserConfig.name.from, nameLength].concat(newNameArray);
			Array.prototype.splice.apply(midiData, args);
			return midiData;
		},
		createPaddedNameArray: function(nameLength, newName){
			var padding = Array(nameLength+1).join(' ');
			newName     = this.pad(padding,newName,false);

			return this.stringToAsciiByteArray(newName);
		},
		pad : function(pad, str, padLeft) {
		  if (typeof str === 'undefined') 
		    return pad;
		  if (padLeft) {
		    return (pad + str).slice(-pad.length);
		  } else {
		    return (str + pad).substring(0, pad.length);
		  }
		},
		stringToAsciiByteArray : function(str) {
			var bytes = [];
			for (var i = 0; i < str.length; ++i)
			{
				var charCode = str.charCodeAt(i);
		      if (charCode > 0xFF)  // char > 1 byte since charCodeAt returns the UTF-16 value
		      {
		      	throw new Error('Character ' + String.fromCharCode(charCode) + ' can\'t be represented by a US-ASCII byte.');
		      }
		      bytes.push(charCode);
		  }
		  return bytes;
		},
		denibble: function(midiData) {
			var denibbledData = [];
			for(var i=0;i<midiData.length; i=i+2) {
				var byte = (midiData[i+1] << 4) | midiData[i];
				denibbledData.push(byte);
			}
			return denibbledData;
		},
		denibbleHighNibbleFirst: function(midiData) {
			var denibbledData = [];
			for(var i=0;i<midiData.length; i=i+2) {
				var byte = (midiData[i] << 4) | midiData[i+1];
				denibbledData.push(byte);
			}
			return denibbledData;
		}
	};
	return obj;
});
