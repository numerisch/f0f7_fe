angular.module('f0f7.controllers', [])
.controller('SysexController', function($scope, $log, $timeout, WebMIDI, Sysex, Devices, RestService, $localStorage, Auth, Flash, $location, $routeParams, config, $q, hotkeys, User, Eloquent, routeNavigation){

	var device = {};
	var progressTerminatorTimer;
	var selectProgramTimer;
	var suggestedBankName = '';

	// Model to JSON for demo purpose
	// $scope.$watch('models', function(models) {
	//     $scope.modelAsJson = angular.toJson(models, true);
	// }, true);

	$scope.requestMidiAccess = function() {
		$localStorage.userHasRequestedMidi = true;
		initWebMIDI();
	};

	function initWebMIDI() {
		if($localStorage.userHasRequestedMidi === true) {
			WebMIDI.connect().then(onMIDIStarted, onMIDISystemError);
		}
	}
	initWebMIDI();

	function preCheckProgramInList() {
		var prgArr = $scope.models.lists[$scope.models.listInFocus];
		var elementPos = -1;
		if(typeof $scope.models.selected !== 'undefined') {
			if(typeof $scope.models.selected.$$hashKey !== 'undefined') {
				elementPos = prgArr.map(function(x) {return x.$$hashKey; }).indexOf($scope.models.selected.$$hashKey);
			}
		}
		if(elementPos >= (prgArr.length)) {
			elementPos = -1;
		}
		return elementPos;
	}

	hotkeys.bindTo($scope)
	.add({
		combo: 'down',
		description: 'navigate down through program list',
		callback: function() {
			var elementPos = preCheckProgramInList();
			if(typeof $scope.models.lists[$scope.models.listInFocus][elementPos+1] === 'undefined') {
				elementPos = -1;
			}
			var program = $scope.models.lists[$scope.models.listInFocus][elementPos+1];
			$scope.selectProgram(program, $scope.models.listInFocus);
		}
	})
	.add({
		combo: 'up',
		description: 'navigate up through program list',
		callback: function() {
			var elementPos = preCheckProgramInList();
			if(typeof $scope.models.lists[$scope.models.listInFocus][elementPos-1] === 'undefined') {
				elementPos = $scope.models.lists[$scope.models.listInFocus].length;
			}
			var program = $scope.models.lists[$scope.models.listInFocus][elementPos-1];
			$scope.selectProgram(program, $scope.models.listInFocus);
		}
	})
	.add({
		combo: 'left',
		description: 'focus on Masterboard',
		callback: function() {
			if($scope.models.lists.A.length > 0) {
				var elementPos = preCheckProgramInList();
				$scope.models.listInFocus = 'A';
				if(typeof $scope.models.lists[$scope.models.listInFocus][elementPos] === 'undefined') {
					elementPos = 0;
				}
				var program = $scope.models.lists[$scope.models.listInFocus][elementPos];
				$scope.selectProgram(program, $scope.models.listInFocus);
			}
		}
	})
	.add({
		combo: 'right',
		description: 'focus on Clipboard',
		callback: function() {
			if($scope.models.lists.B.length > 0) {
				var elementPos = preCheckProgramInList();
				$scope.models.listInFocus = 'B';
				if(typeof $scope.models.lists[$scope.models.listInFocus][elementPos] === 'undefined') {
					elementPos = 0;
				}
				var program = $scope.models.lists[$scope.models.listInFocus][elementPos];
				$scope.selectProgram(program, $scope.models.listInFocus);
			}
		}
	})
	.add({
		combo: 'backspace',
		description: 'remove program from list',
		callback: function(e) {
			e.preventDefault();
			var elementPos = preCheckProgramInList();
			if(typeof $scope.models.lists[$scope.models.listInFocus][elementPos] !== 'undefined') {
				$scope.models.lists[$scope.models.listInFocus].splice(elementPos, 1);
				if(typeof $scope.models.lists[$scope.models.listInFocus][elementPos] !== 'undefined') {
					var program = $scope.models.lists[$scope.models.listInFocus][elementPos];
					$scope.selectProgram(program, $scope.models.listInFocus);
				}
			}
		}
	})
	.add({
		combo: 'space',
		description: 'move program to other list',
		callback: function(e) {
			e.preventDefault();
			var elementPos = preCheckProgramInList();
			if(typeof $scope.models.lists[$scope.models.listInFocus][elementPos] !== 'undefined') {
				if($scope.models.listInFocus == 'A') {
					$scope.models.lists.B.push($scope.models.lists[$scope.models.listInFocus][elementPos]);
				} else {
					$scope.models.lists.A.push($scope.models.lists[$scope.models.listInFocus][elementPos]);
				}
				$scope.models.lists[$scope.models.listInFocus].splice(elementPos, 1);
				if(typeof $scope.models.lists[$scope.models.listInFocus][elementPos] !== 'undefined') {
					var program = $scope.models.lists[$scope.models.listInFocus][elementPos];
					$scope.selectProgram(program, $scope.models.listInFocus);
				}
			}
		}
	});

	$scope.getBankmanagerData = function() {
		$scope.getPrivateBankmanagerData();
		$scope.getPublicBankmanagerData();
	};

	$scope.getPrivateBankmanagerData = function(page, selectId) {
		if(typeof $scope.tokenClaims.sub != 'undefined') {
			page = typeof page == 'undefined' ? 1 : page;
			selectId = typeof selectId == 'undefined' ? null : selectId;
			try {
				RestService.listPrivateBanks({ngclass: device.id, page: page}, function (banks) {
					$scope.bankmanager.private = banks;
					$scope.bankmanager.private.selected = selectId;
					if ($scope.user.is_valid_pro === true) { //todo: listen on user change
						$scope.bankmanager.limit = config.accounts.pro.bankmanager.limit;
					}
				}, httpError);
			}
			catch (e) {
				console.warn(e.toString());
			}
		}
	};

	$scope.getPublicBankmanagerData = function(page) {
		page = typeof page == 'undefined' ? 1 : page;
		try {
			RestService.listPublicBanks({ngclass: device.id, page: page}, function (banks) {
				$scope.bankmanager.public = banks;
				$scope.bankmanager.public.selected = null;
			}, httpError);
		}
		catch (e) {
			console.warn(e.toString());
		}
	};

	$scope.togglePublishBank = function(index, bank) {
		$scope.bankmanager.private.data[index].is_public = bank.is_public == 1 ? 0 : 1;
		var data = {
			id : bank.id,
			is_public : $scope.bankmanager.private.data[index].is_public
		};

		RestService.editBank(data, $scope.getPublicBankmanagerData, httpError);
	};

	$scope.deleteBank = function(bank) {
		RestService.deleteBank(bank.id, $scope.getBankmanagerData, httpError);
	};

	$scope.editBankName = function(data, bankId) {
		var d = $q.defer();
		RestService.editBank({id : bankId, name : data}, function() {
			d.resolve();
			$scope.getPublicBankmanagerData();
		}, function(res) {
			var errorObj = res.data.errors.name;
			d.resolve(errorObj[Object.keys(errorObj)[0]]);
		});
		return d.promise;
	};

	$scope.loadBank = function(bank, patchListId) {
		if(patchListId === 'A') {
			$scope.bankmanager.private.selected = bank.id;
		} else {
			$scope.bankmanager.public.selected = bank.id;
		}

		RestService.getBank(bank.id, function(res) {
			var programs = [];
			for(var i = 0; i < res.data.programs.length; i++) {
				programs[i] = {
					name :res.data.programs[i].name.trim(),
					dna : res.data.programs[i].dna,
					data : angular.fromJson(res.data.programs[i].data),
				};
			}
			$scope.models.lists[patchListId] = programs;
		}, httpError);
	};

	$scope.changeDevice = function() {
		device = Devices.setDevice($scope.selectedDevice);
		if(typeof device.id == 'undefined') {
			$location.path('/');
			return;
		}
		$log.log(device.manuf + ' ' + device.model);

		getAndSetBanks();

		$scope.models.lists.A = [];
		$scope.models.lists.B = [];
		Flash.clear();

		$scope.getBankmanagerData();
	};

	$scope.go = function() {
		$location.path('/SysexLibrarian/' + $scope.selectedDevice);
	};

	$scope.setDeviceFromRouteParams = function() {
		if(typeof $routeParams.deviceId != 'undefined') {
			$scope.selectedDevice = $routeParams.deviceId;
			$scope.changeDevice();
		}
		$log.log($routeParams.deviceId);
	};

	$scope.selectProgram = function(program, patchListId) {
		$scope.models.listInFocus = patchListId;
		$scope.models.selected = program;

		if(selectProgramTimer) {
			$log.log("Someone is selecting programs like mad.");
			$timeout.cancel(selectProgramTimer);
		}

		selectProgramTimer = $timeout(function() {
			$log.log("O.k. now I am sending the MIDIs");
			$log.log('program ' , program.name);

			if(WebMIDI.midiOut.state == 'connected') {
				device.sendToBuffer(program);
				$scope.logger = Sysex.params.logData;
				Sysex.params.logData = '';
				
			} else {
				alertMessage('No MIDI out connected.');
				return;
			}

		}, 300);
	};

	$scope.editProgramName = function(patchListId, newName, programId) {
		var maxNameLength = device.getMaxNameLength();
		if(newName == "RaND") {
			newName = Eloquent.phrase(maxNameLength);
		} else {
			if(newName.length > maxNameLength) {
				return "No more than " + maxNameLength + " characters.";
			}
			if(!/^[\x00-\x7F]*$/.test(newName)) {
				return "You cannot use special chars.";
			}
		}
		$scope.models.lists[patchListId][programId] = device.renameProgram(newName, $scope.models.lists[patchListId][programId]);
		$scope.logger = Sysex.params.logData;
	};

	$scope.fillBankWithThisProgram = function(patchListId, programId) {
		var programs = $scope.models.lists[patchListId];
		var srcProgram = programs[programId];
		var programsInList = programs.length;

		delete(srcProgram.$$hashKey);

		for(var i= programsInList; i< device.params.patchesPerBank; i++) {
			var clone = Object.assign({}, srcProgram);
			clone.cloneindex = i;
			programs.push(clone);
		}

		$scope.models.lists[patchListId] = programs;
	};

	$scope.swapLists = function() {
		var tempList = $scope.models.lists.A;
		$scope.models.lists.A = $scope.models.lists.B;
		$scope.models.lists.B = tempList;
	};

	$scope.clearList = function(patchListId) {
		$scope.models.lists[patchListId] = [];
	};

	$scope.retrieveBank = function(patchListId) {
		if(WebMIDI.midiOut.state == 'connected') {
			$scope.models.lists.A = [];
			if($scope.amountOfBanks > 1) {
				suggestedBankName = $scope.banks[$scope.selectedBank].label;
			}
            routeNavigation.progressbar.percentage = 1;
			device.retrieveBank($scope.banks[$scope.selectedBank].address);
		} else {
			alertMessage('No MIDI out connected.');
		}
	};

	$scope.sendBank = function(patchListId) {

		Sysex.params.logData = '';

		var programList = $scope.models.lists[patchListId];
		var qtyOfProgramsToTransmit = programList.length;
		var bank = $scope.banks[$scope.selectedBank].address;
		var prgNo = 0;
		var program;
		var i = 0;

		if(qtyOfProgramsToTransmit > device.params.patchesPerBank) {
			qtyOfProgramsToTransmit = device.params.patchesPerBank;
		}

		var delayedProgramSend = function(prgNo) {
			$log.log(prgNo + ' / ' + qtyOfProgramsToTransmit);

			program = programList[prgNo];
			if(typeof program == 'undefined') {
				$log.error('Dump Error');
				routeNavigation.progressbar.percentage = 0;
			}
			device.sendProgramToBank(program, bank, prgNo);

			routeNavigation.progressbar.percentage = Math.round((prgNo+1) * 100 / qtyOfProgramsToTransmit);

			$scope.logger = Sysex.params.logData;
			Sysex.params.logData = '';

			if(prgNo < (qtyOfProgramsToTransmit-1)) {
				prgNo++;
				$timeout(delayedProgramSend, device.params.transferPause, true, prgNo);
			} else {
				routeNavigation.progressbar.percentage = 100;
				$timeout(function() {routeNavigation.progressbar.percentage = 0;}, 500, true);
				alertMessage('All programs transferred.', 'success');
			}
		};

		delayedProgramSend(prgNo);
	};

	$scope.sendProgram = function(program, prgNo) {
		var bank = $scope.banks[$scope.selectedBank].address;
		device.sendProgramToBank(program, bank, prgNo);
		$scope.logger = Sysex.params.logData;
		Sysex.params.logData = '';
		alertMessage('Programs transferred.', 'success');
	};

	$scope.downloadBank = function(patchListId) {
		var programList = $scope.models.lists[patchListId];
		var amountOfPatchesInList = programList.length;
		var bank = $scope.banks[$scope.selectedBank].address;
		var prgNo = 0;
		var blobData = [];

		for(prgNo = 0; prgNo < amountOfPatchesInList; prgNo++ ) {
			blobData = blobData.concat(device.reorderProgram(programList[prgNo], bank, prgNo));
		}

		var arrayUint8 = Sysex.convertToUint8(blobData);
		var downloadFile =  new Blob([arrayUint8], {type: "application/octet-stream"});
		saveAs(downloadFile, "f0f7_" + device.model + "_bank.syx");
	};

	$scope.saveBank = function(patchListId) {
		var bankName;
		if(suggestedBankName.length > 0) {
			bankName = suggestedBankName;
			suggestedBankName = '';
		} else {
			bankName = 'My ' + device.model + ' Bank';
		}
		RestService.saveBank({
			bank : {
				name : bankName,
				is_public : 0,
				ngclass : device.id,
			},
			programs : $scope.models.lists[patchListId]
		}, function(res) {
			$scope.getPrivateBankmanagerData(1, res.data.id);
			alertMessage('Bank saved!', 'success');
		}, httpError);
	};

	$scope.updateProgramsInBank = function() {
		RestService.updateProgramsInBank({
			bank : {
				id : $scope.bankmanager.private.selected,
				ngclass : device.id,
			},
			programs : $scope.models.lists.A
		}, function(res) {
			if(res.success === true) {
				alertMessage('Bank updated!', 'success');
			} else {
				alertMessage('An error occured while updating the Bank.', 'danger');
			}
		}, httpError);
	};

	$scope.processSysexFile = function(event) {
		var input = event.target;

		var reader = new FileReader();
		if(typeof input.files[0].name != 'undefined') {
			suggestedBankName = input.files[0].name;
		}

		reader.onloadend = function(){
			var raw = Sysex.decodeDataURI(reader.result);
			var md = [];
			var byte;
			var i = 0;
			var thisIsSysExData = false;

			if(raw.charCodeAt(0) != 0xF0) {
				alertMessage('Caution, this file may contain invalid Sysex data. LASER Mammoth will still try to process what is there.');
				// return;
			} else {
				Flash.clear();
			}

			for(i = 0; i < raw.length; i++) {
				byte = raw.charCodeAt(i);
				if(byte == 0xF0) {
					thisIsSysExData = true;
				}
				if(thisIsSysExData === true) {
					md.push(byte);
				}
				if(byte == 0xF7) {
					md = Sysex.convertToUint8(md); //important for dna
					extractPatchNames(md, 'B');
					md = [];
					thisIsSysExData = false;
				}
			}
		};
		reader.readAsDataURL(input.files[0]);
	};

	$scope.safeApply = function(fn) {
		var phase = this.$root.$$phase;
		if(phase == '$apply' || phase == '$digest') {
			if(fn && (typeof(fn) === 'function')) {
				fn();
			}
		} else {
			this.$apply(fn);
		}
	};

	function onMIDIStarted(midiAccess) {
		$scope.midiAvailable = true;
		WebMIDI.midiAccess = midiAccess;

		function selectFirstAvailableInPort() {
			$log.log('selectFirstAvailableInPort');
			return WebMIDI.selectFirstAvailableInPort(function(port) {
				$log.log('first inport is ' + port.value.name);
				$scope.midiInPort.id = port.value.id;
				$scope.changeMIDIInPort();
			}, function() {
				$scope.midiInPort.id = null;
				$log.log("no midiInPort available");
			});
		}

		function selectFirstAvailableOutPort() {
			$log.log('selectFirstAvailableOutPort');
			return WebMIDI.selectFirstAvailableOutPort(function(port) {
				$log.log('first outport is ' + port.value.name);
				$scope.midiOutPort.id = port.value.id;
				$scope.changeMIDIOutPort();
			}, function() {
				$scope.midiOutPort.id = null;
				$log.log("no midiOutPort available");
			});
		}

		function getAndSelectPorts() {
			$scope.midiInPort.availableOptions = WebMIDI.getAvailableInPorts();
			$scope.changeMIDIInPort();
			$scope.midiOutPort.availableOptions = WebMIDI.getAvailableOutPorts();
			$scope.changeMIDIOutPort();
		}

		$scope.changeMIDIInPort = function() {
			WebMIDI.selectInPort($scope.midiInPort.id, midiMessageReceived, selectFirstAvailableInPort);
		};

		$scope.changeMIDIOutPort = function() {
			WebMIDI.selectOutPort($scope.midiOutPort.id, selectFirstAvailableOutPort);
		};

		$scope.midiInPort = {};
		$scope.midiOutPort = {};

		if($localStorage.midiInPortId) {
			$scope.midiInPort.id = $localStorage.midiInPortId;
		}
		if($localStorage.midiOutPortId) {
			$scope.midiOutPort.id = $localStorage.midiOutPortId;
		}

		getAndSelectPorts();

		WebMIDI.midiAccess.onstatechange = function(e) {
			var msg, msgClass;
			if(e.port) {
				msg = e.port.manufacturer + ' ' + e.port.name + ' ' + e.port.state; 
				msgClass = e.port.state == 'connected' ? 'success' : 'warning';
 			} else {
 				msg = 'Interface changed';
 			}
 			getAndSelectPorts();
			alertMessage(msg, msgClass);
		};
	}

	function onMIDISystemError( msg ) {
		msg = msg + '<hr/><p>Your browser must support the <em>Web MIDI API</em> which is neccessary to run LASER Mammoth.<br/>If you have blocked the access accidently you can unblock it by clicking on the icon top left in the browser addressbar.<br />If you are using <em>Firefox</em> click on this <a href="static/site_permissions_for_f0f7net-1.0-an+fx.xpi">link to install this permission add-on</a> and reload the page.</p>';
		$log.log( "Error encountered:" + msg );
		alertMessage(msg, 'danger', 0);
	}

	function midiMessageReceived(ev) {
		$log.info("midi Message Received on " + ev.target.name);
		midiMonitor(ev.data);
		extractPatchNames(ev.data, 'A');
	}

	function extractPatchNames(md, patchListId) {
		if(md[0] == 0xF0) {
			if (typeof device.id == 'undefined') {
				device = Devices.detect(md);
                if (!("id" in device)) {
					alertMessage('Sysex device is not supported.');
					return;
				}
				$scope.selectedDevice = device.id;
				$scope.changeDevice();

			} else {
				var tmpDevice = Devices.detect(md);
				if(tmpDevice.id != device.id) {
					alertMessage('Received Sysex dump from ' + tmpDevice.model + ' but ' + device.model + ' is already set as device.');
					return;
				}
			}

			var patches = device.extractPatchNames(md);
			if(patches.length > 0) {
				$scope.$apply(function() {

					//more programs than space in bank? clear list.
					// if($scope.models.lists[patchListId].length >= device.params.patchesPerBank) {
					// 	$scope.models.lists[patchListId] = [];
					// }

					for(var i=0; i<patches.length; i++) {
						// patches[i].cloneindex = i;
						$scope.models.lists[patchListId].push(patches[i]);
					}

					//trigger progressbar
					var programsInList = $scope.models.lists[patchListId].length;
					routeNavigation.progressbar.percentage = Math.ceil((programsInList+1) * 100 / device.params.patchesPerBank);

					if(progressTerminatorTimer) {
						$timeout.cancel(progressTerminatorTimer);
					}
					progressTerminatorTimer = $timeout(function() {
						$log.log("no more incoming events? I end progressbar.");
						finishProgresssbar(); 
						alertMessage("I guess everything is fine but I expected a different amount of programs.", 'warning');
					}, 8000);

					if(programsInList == device.params.patchesPerBank) {
						$timeout.cancel(progressTerminatorTimer);
						finishProgresssbar();
					}

					$scope.logger = Sysex.params.logData;
				});
			}
		}
	}

	function finishProgresssbar() {
		routeNavigation.progressbar.percentage = 100;
		$timeout(function() {routeNavigation.progressbar.percentage = 0;}, 500, true);
		Flash.clear();
	}

	function alertMessage(message, msgClass, flashTimer) {
		Flash.clear();
		if(typeof msgClass == 'undefined') {
			msgClass = 'warning';
		}
		if(typeof flashTimer == 'undefined') {
			flashTimer = 10000;
		}
		$scope.safeApply(function() {
			Flash.create(msgClass, message, flashTimer, {class: 'alert-top'});
		});
	}

	function getAndSetBanks() {
		if(typeof device.params != 'undefined') {
			$scope.banks = device.params.banks;
			$scope.selectedBank = Object.keys($scope.banks)[0];

			$scope.patchesPerBank = device.params.patchesPerBank;
			$scope.programsCanBeRenamed = typeof device.params.parserConfig.name !== 'undefined';
			$scope.singleProgramsCannotBeTransmitted = typeof device.params.singleProgramsCannotBeTransmitted !== 'undefined';
            $scope.banksCannotBeRequested = typeof device.params.banksCannotBeRequested !== 'undefined';

			$scope.deviceModel = device.model;
			$scope.deviceHints = device.hints;

			$scope.amountOfBanks = Object.keys($scope.banks).length;
		} else {
			initScope();
		}
	}

	function midiMonitor(midiData) {
		var logData = Array.from(midiData);
        // var logData = Array.from(midiData).splice(0, 24);
		for(var i=0; i<logData.length; i++) {
			logData[i] = Sysex.byteToHex(logData[i]);
		}
		var logStr = logData.join(' ');
		// alertMessage("Incoming MIDI data: " + logStr +'...', 'info');
        $scope.logger = logStr;
		
		$scope.safeApply(function() {
			$scope.shootLasers = true;
			$timeout(function() {
				$scope.shootLasers = false;
			}, 300, true);
		});

	}

	function httpError(res) {
		var message = ((res || {}).data || {}).message || {};
		if(typeof message == 'object') {
			message = res.message;
		}
		if(message == 'Expired token') {
			Auth.logout(function () {
				$location.path('/login');
			});
		}
		alertMessage(message, 'danger');
	}

	function initScope() {
		$scope.token = $localStorage.token;
		$scope.tokenClaims = Auth.getTokenClaims();

		$scope.availableDevices = Devices.getOptions();
		$scope.selectedDevice = null;

		$scope.banks = {};
		$scope.selectedBank = null;

		$scope.patchesPerBank = null;
		$scope.programsCanBeRenamed = false;
		$scope.singleProgramsCannotBeTransmitted = false;
        $scope.banksCannotBeRequested = false;
        
		$scope.deviceModel = 'unknown';
		$scope.deviceHints = [];
		
		$scope.amountOfBanks = 0;

		$scope.models = {
		    selected: {},
		    listInFocus : 'A',
		    lists: {"A": [], "B": []}
		};

		$scope.logger = '';

		$scope.bankmanager = {
			limit : config.accounts.standard.bankmanager.limit,
			public : {
				selected : null,
				data : {}
			},
			private : {
				selected : null,
				data : {}
			},
		};

		$scope.setDeviceFromRouteParams();

		$scope.user = User;
		// if(User.is_valid_pro === true) {
		// 	$scope.bankmanager.limit = config.accounts.pro.bankmanager.limit;
		// }

		$scope.showHelp = true;
        $scope.showMonitor = true;
        $scope.shootLasers = false;

		// $scope.banks = {
		// 	options : {},
		// 	selected : null,
		// 	qty : 0,
		// 	patches : 0
		// }

		// for (var i = 1; i <= 3; ++i) {
		//     $scope.models.lists.A.push({name: "Item A" + i, program : [10,11,12]});
		//     $scope.models.lists.B.push({name: "Item B" + i, program : [10,11,12]});
		// }
	}
	initScope();

    window.onbeforeunload = function() {
        if($scope.models.lists.A.length > 0) {
            return true;
        }
    };
    
	Flash.clear();
});
