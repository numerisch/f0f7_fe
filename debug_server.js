/*jshint esversion: 6 */
const express = require('express')
const app = express()

//app.use("/src", express.static(__dirname + "/www/fe/src"));
app.use("/src", express.static(__dirname + "/src"));
app.use("/www", express.static(__dirname + "/www"));

app.listen(3000)
